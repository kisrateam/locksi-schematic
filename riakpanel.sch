<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="6" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Patch_Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Mittellin" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="Stiffner" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tBridges" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="tBPL" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="bBPL" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="MPL" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="117" name="PM_Ref" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="PF_Ref" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="WFL_Ref" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="sName" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bPlace" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="218" name="218bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="219" name="219bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="220" name="220bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="221" name="221bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="222" name="222bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="223" name="223bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="224" name="224bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="relay16channel">
<packages>
<package name="RELAY16">
<pad name="4" x="-1.27" y="2.54" drill="0.6" shape="square"/>
<pad name="5" x="1.27" y="2.54" drill="0.6" shape="square"/>
<pad name="6" x="3.81" y="2.54" drill="0.6" shape="square"/>
<pad name="7" x="6.35" y="2.54" drill="0.6" shape="square"/>
<pad name="8" x="8.89" y="2.54" drill="0.6" shape="square"/>
<pad name="18" x="8.89" y="5.08" drill="0.6" shape="square"/>
<pad name="17" x="6.35" y="5.08" drill="0.6" shape="square"/>
<pad name="16" x="3.81" y="5.08" drill="0.6" shape="square"/>
<pad name="15" x="1.27" y="5.08" drill="0.6" shape="square"/>
<pad name="14" x="-1.27" y="5.08" drill="0.6" shape="square"/>
<pad name="13" x="-3.81" y="5.08" drill="0.6" shape="square"/>
<pad name="3" x="-3.81" y="2.54" drill="0.6" shape="square"/>
<pad name="12" x="-6.35" y="5.08" drill="0.6" shape="square"/>
<pad name="2" x="-6.35" y="2.54" drill="0.6" shape="square"/>
<pad name="19" x="11.43" y="5.08" drill="0.6" shape="square"/>
<pad name="9" x="11.43" y="2.54" drill="0.6" shape="square"/>
<pad name="20" x="13.97" y="5.08" drill="0.6" shape="square"/>
<pad name="10" x="13.97" y="2.54" drill="0.6" shape="square"/>
<pad name="11" x="-8.89" y="5.08" drill="0.6" shape="square"/>
<pad name="1" x="-8.89" y="2.54" drill="0.6" shape="square"/>
<pad name="VCC" x="-1.27" y="11.43" drill="0.6" shape="square"/>
<pad name="GND" x="1.27" y="11.43" drill="0.6" shape="square"/>
<pad name="23" x="-20.32" y="20.32" drill="0.6" shape="square"/>
<pad name="24" x="-17.78" y="20.32" drill="0.6" shape="square"/>
<pad name="34" x="-20.32" y="17.78" drill="0.6" shape="square"/>
<pad name="35" x="-17.78" y="17.78" drill="0.6" shape="square"/>
<pad name="41" x="-20.32" y="15.24" drill="0.6" shape="square"/>
<pad name="42" x="-17.78" y="15.24" drill="0.6" shape="square"/>
<pad name="51" x="-20.32" y="12.7" drill="0.6" shape="square"/>
<pad name="52" x="-16.51" y="12.7" drill="0.6" shape="square"/>
<pad name="55" x="-20.32" y="10.16" drill="0.6" shape="square"/>
<pad name="56" x="-17.78" y="10.16" drill="0.6" shape="square"/>
<pad name="59" x="-20.32" y="7.62" drill="0.6" shape="square"/>
<pad name="60" x="-17.78" y="7.62" drill="0.6" shape="square"/>
<pad name="63" x="-20.32" y="5.08" drill="0.6" shape="square"/>
<pad name="64" x="-17.78" y="5.08" drill="0.6" shape="square"/>
<pad name="67" x="-20.32" y="2.54" drill="0.6" shape="square"/>
<pad name="68" x="-17.78" y="2.54" drill="0.6" shape="square"/>
<pad name="22" x="-22.86" y="20.32" drill="0.6" shape="square"/>
<pad name="21" x="-25.4" y="20.32" drill="0.6" shape="square"/>
<pad name="32" x="-25.4" y="17.78" drill="0.6" shape="square"/>
<pad name="33" x="-22.86" y="17.78" drill="0.6" shape="square"/>
<pad name="39" x="-25.4" y="15.24" drill="0.6" shape="square"/>
<pad name="40" x="-22.86" y="15.24" drill="0.6" shape="square"/>
<pad name="49" x="-25.4" y="12.7" drill="0.6" shape="square"/>
<pad name="50" x="-22.86" y="12.7" drill="0.6" shape="square"/>
<pad name="53" x="-25.4" y="10.16" drill="0.6" shape="square"/>
<pad name="54" x="-22.86" y="10.16" drill="0.6" shape="square"/>
<pad name="57" x="-25.4" y="7.62" drill="0.6" shape="square"/>
<pad name="58" x="-22.86" y="7.62" drill="0.6" shape="square"/>
<pad name="61" x="-25.4" y="5.08" drill="0.6" shape="square"/>
<pad name="62" x="-22.86" y="5.08" drill="0.6" shape="square"/>
<pad name="65" x="-25.4" y="2.54" drill="0.6" shape="square"/>
<pad name="66" x="-22.86" y="2.54" drill="0.6" shape="square"/>
<pad name="25" x="-15.24" y="20.32" drill="0.6" shape="square"/>
<pad name="29" x="-15.24" y="19.05" drill="0.6" shape="square"/>
<pad name="36" x="-15.24" y="17.78" drill="0.6" shape="square"/>
<pad name="26" x="-13.97" y="20.32" drill="0.6" shape="square"/>
<pad name="30" x="-13.97" y="19.05" drill="0.6" shape="square"/>
<pad name="37" x="-13.97" y="17.78" drill="0.6" shape="square"/>
<pad name="27" x="-12.7" y="20.32" drill="0.6" shape="square"/>
<pad name="31" x="-12.7" y="19.05" drill="0.6" shape="square"/>
<pad name="38" x="-12.7" y="17.78" drill="0.6" shape="square"/>
<pad name="28" x="-11.43" y="20.32" drill="0.6" shape="square"/>
<pad name="43" x="-13.97" y="15.24" drill="0.6" shape="square"/>
<pad name="44" x="-11.43" y="15.24" drill="0.6" shape="square"/>
<pad name="45" x="-8.89" y="15.24" drill="0.6" shape="square"/>
<pad name="46" x="-6.35" y="15.24" drill="0.6" shape="square"/>
<pad name="47" x="-3.81" y="15.24" drill="0.6" shape="square"/>
<pad name="48" x="-2.54" y="15.24" drill="0.6" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="RELAY16CHANNEL">
<pin name="VCC.1" x="-20.32" y="-2.54" length="middle" rot="R90"/>
<pin name="VCC.2" x="-17.78" y="-2.54" length="middle" rot="R90"/>
<pin name="IN1" x="-15.24" y="-2.54" length="middle" rot="R90"/>
<pin name="IN2" x="-12.7" y="-2.54" length="middle" rot="R90"/>
<pin name="IN3" x="-10.16" y="-2.54" length="middle" rot="R90"/>
<pin name="IN4" x="-7.62" y="-2.54" length="middle" rot="R90"/>
<pin name="IN5" x="-5.08" y="-2.54" length="middle" rot="R90"/>
<pin name="IN6" x="-2.54" y="-2.54" length="middle" rot="R90"/>
<pin name="IN7" x="0" y="-2.54" length="middle" rot="R90"/>
<pin name="IN8" x="2.54" y="-2.54" length="middle" rot="R90"/>
<pin name="IN9" x="5.08" y="-2.54" length="middle" rot="R90"/>
<pin name="IN10" x="7.62" y="-2.54" length="middle" rot="R90"/>
<pin name="IN11" x="10.16" y="-2.54" length="middle" rot="R90"/>
<pin name="IN12" x="12.7" y="-2.54" length="middle" rot="R90"/>
<pin name="IN13" x="15.24" y="-2.54" length="middle" rot="R90"/>
<pin name="IN14" x="17.78" y="-2.54" length="middle" rot="R90"/>
<pin name="IN15" x="20.32" y="-2.54" length="middle" rot="R90"/>
<pin name="IN16" x="22.86" y="-2.54" length="middle" rot="R90"/>
<pin name="GND.1" x="25.4" y="-2.54" length="middle" rot="R90"/>
<pin name="GND.2" x="27.94" y="-2.54" length="middle" rot="R90"/>
<wire x1="-25.4" y1="2.54" x2="33.02" y2="2.54" width="0.254" layer="94"/>
<wire x1="33.02" y1="2.54" x2="33.02" y2="12.7" width="0.254" layer="94"/>
<wire x1="33.02" y1="12.7" x2="33.02" y2="17.78" width="0.254" layer="94"/>
<wire x1="33.02" y1="17.78" x2="33.02" y2="22.86" width="0.254" layer="94"/>
<wire x1="33.02" y1="22.86" x2="33.02" y2="27.94" width="0.254" layer="94"/>
<wire x1="-25.4" y1="2.54" x2="-25.4" y2="12.7" width="0.254" layer="94"/>
<wire x1="-25.4" y1="12.7" x2="-25.4" y2="17.78" width="0.254" layer="94"/>
<wire x1="-25.4" y1="17.78" x2="-25.4" y2="22.86" width="0.254" layer="94"/>
<wire x1="-25.4" y1="22.86" x2="-25.4" y2="27.94" width="0.254" layer="94"/>
<wire x1="-25.4" y1="12.7" x2="-17.78" y2="12.7" width="0.254" layer="94"/>
<wire x1="-17.78" y1="12.7" x2="-17.78" y2="15.24" width="0.254" layer="94"/>
<wire x1="-17.78" y1="15.24" x2="-17.78" y2="17.78" width="0.254" layer="94"/>
<wire x1="-25.4" y1="17.78" x2="-17.78" y2="17.78" width="0.254" layer="94"/>
<wire x1="-17.78" y1="17.78" x2="-17.78" y2="20.32" width="0.254" layer="94"/>
<wire x1="-17.78" y1="20.32" x2="-17.78" y2="22.86" width="0.254" layer="94"/>
<wire x1="-25.4" y1="22.86" x2="-17.78" y2="22.86" width="0.254" layer="94"/>
<wire x1="-17.78" y1="22.86" x2="-17.78" y2="25.4" width="0.254" layer="94"/>
<wire x1="-25.4" y1="27.94" x2="-17.78" y2="27.94" width="0.254" layer="94"/>
<wire x1="-17.78" y1="27.94" x2="-17.78" y2="25.4" width="0.254" layer="94"/>
<wire x1="-17.78" y1="25.4" x2="-12.7" y2="25.4" width="0.254" layer="94"/>
<wire x1="-12.7" y1="25.4" x2="-12.7" y2="22.86" width="0.254" layer="94"/>
<wire x1="-17.78" y1="20.32" x2="-15.24" y2="20.32" width="0.254" layer="94"/>
<wire x1="-17.78" y1="15.24" x2="-12.7" y2="15.24" width="0.254" layer="94"/>
<wire x1="-12.7" y1="15.24" x2="-12.7" y2="17.78" width="0.254" layer="94"/>
<wire x1="33.02" y1="12.7" x2="25.4" y2="12.7" width="0.254" layer="94"/>
<wire x1="25.4" y1="12.7" x2="25.4" y2="15.24" width="0.254" layer="94"/>
<wire x1="25.4" y1="15.24" x2="25.4" y2="17.78" width="0.254" layer="94"/>
<wire x1="33.02" y1="17.78" x2="25.4" y2="17.78" width="0.254" layer="94"/>
<wire x1="25.4" y1="17.78" x2="25.4" y2="20.32" width="0.254" layer="94"/>
<wire x1="25.4" y1="20.32" x2="25.4" y2="22.86" width="0.254" layer="94"/>
<wire x1="33.02" y1="22.86" x2="25.4" y2="22.86" width="0.254" layer="94"/>
<wire x1="25.4" y1="22.86" x2="25.4" y2="25.4" width="0.254" layer="94"/>
<wire x1="33.02" y1="27.94" x2="25.4" y2="27.94" width="0.254" layer="94"/>
<wire x1="25.4" y1="27.94" x2="25.4" y2="25.4" width="0.254" layer="94"/>
<wire x1="25.4" y1="25.4" x2="20.32" y2="25.4" width="0.254" layer="94"/>
<wire x1="20.32" y1="25.4" x2="20.32" y2="22.86" width="0.254" layer="94"/>
<wire x1="25.4" y1="20.32" x2="22.86" y2="20.32" width="0.254" layer="94"/>
<wire x1="25.4" y1="15.24" x2="20.32" y2="15.24" width="0.254" layer="94"/>
<wire x1="20.32" y1="15.24" x2="20.32" y2="17.78" width="0.254" layer="94"/>
<wire x1="-25.4" y1="27.94" x2="-25.4" y2="33.02" width="0.254" layer="94"/>
<wire x1="-25.4" y1="33.02" x2="-25.4" y2="38.1" width="0.254" layer="94"/>
<wire x1="-25.4" y1="38.1" x2="-25.4" y2="43.18" width="0.254" layer="94"/>
<wire x1="33.02" y1="27.94" x2="33.02" y2="33.02" width="0.254" layer="94"/>
<wire x1="33.02" y1="33.02" x2="33.02" y2="38.1" width="0.254" layer="94"/>
<wire x1="33.02" y1="38.1" x2="33.02" y2="43.18" width="0.254" layer="94"/>
<wire x1="-25.4" y1="33.02" x2="-17.78" y2="33.02" width="0.254" layer="94"/>
<wire x1="-17.78" y1="33.02" x2="-17.78" y2="35.56" width="0.254" layer="94"/>
<wire x1="-17.78" y1="35.56" x2="-17.78" y2="38.1" width="0.254" layer="94"/>
<wire x1="-17.78" y1="38.1" x2="-25.4" y2="38.1" width="0.254" layer="94"/>
<wire x1="-17.78" y1="38.1" x2="-17.78" y2="40.64" width="0.254" layer="94"/>
<wire x1="-17.78" y1="40.64" x2="-17.78" y2="43.18" width="0.254" layer="94"/>
<wire x1="-17.78" y1="43.18" x2="-25.4" y2="43.18" width="0.254" layer="94"/>
<wire x1="-25.4" y1="43.18" x2="-25.4" y2="48.26" width="0.254" layer="94"/>
<wire x1="-17.78" y1="43.18" x2="-17.78" y2="45.72" width="0.254" layer="94"/>
<wire x1="-17.78" y1="45.72" x2="-17.78" y2="48.26" width="0.254" layer="94"/>
<wire x1="-17.78" y1="48.26" x2="-25.4" y2="48.26" width="0.254" layer="94"/>
<wire x1="-17.78" y1="45.72" x2="-12.7" y2="45.72" width="0.254" layer="94"/>
<wire x1="-12.7" y1="45.72" x2="-12.7" y2="43.18" width="0.254" layer="94"/>
<wire x1="-17.78" y1="40.64" x2="-15.24" y2="40.64" width="0.254" layer="94"/>
<wire x1="-17.78" y1="35.56" x2="-12.7" y2="35.56" width="0.254" layer="94"/>
<wire x1="-12.7" y1="35.56" x2="-12.7" y2="38.1" width="0.254" layer="94"/>
<wire x1="33.02" y1="33.02" x2="25.4" y2="33.02" width="0.254" layer="94"/>
<wire x1="25.4" y1="33.02" x2="25.4" y2="35.56" width="0.254" layer="94"/>
<wire x1="25.4" y1="35.56" x2="25.4" y2="38.1" width="0.254" layer="94"/>
<wire x1="25.4" y1="38.1" x2="33.02" y2="38.1" width="0.254" layer="94"/>
<wire x1="25.4" y1="38.1" x2="25.4" y2="40.64" width="0.254" layer="94"/>
<wire x1="25.4" y1="40.64" x2="25.4" y2="43.18" width="0.254" layer="94"/>
<wire x1="25.4" y1="43.18" x2="33.02" y2="43.18" width="0.254" layer="94"/>
<wire x1="33.02" y1="43.18" x2="33.02" y2="48.26" width="0.254" layer="94"/>
<wire x1="33.02" y1="48.26" x2="25.4" y2="48.26" width="0.254" layer="94"/>
<wire x1="25.4" y1="48.26" x2="25.4" y2="45.72" width="0.254" layer="94"/>
<wire x1="25.4" y1="45.72" x2="25.4" y2="43.18" width="0.254" layer="94"/>
<wire x1="25.4" y1="45.72" x2="20.32" y2="45.72" width="0.254" layer="94"/>
<wire x1="20.32" y1="45.72" x2="20.32" y2="43.18" width="0.254" layer="94"/>
<wire x1="25.4" y1="40.64" x2="22.86" y2="40.64" width="0.254" layer="94"/>
<wire x1="25.4" y1="35.56" x2="20.32" y2="35.56" width="0.254" layer="94"/>
<wire x1="20.32" y1="35.56" x2="20.32" y2="38.1" width="0.254" layer="94"/>
<wire x1="-25.4" y1="48.26" x2="-25.4" y2="53.34" width="0.254" layer="94"/>
<wire x1="-25.4" y1="53.34" x2="-25.4" y2="58.42" width="0.254" layer="94"/>
<wire x1="-25.4" y1="58.42" x2="-25.4" y2="63.5" width="0.254" layer="94"/>
<wire x1="33.02" y1="48.26" x2="33.02" y2="53.34" width="0.254" layer="94"/>
<wire x1="33.02" y1="53.34" x2="33.02" y2="58.42" width="0.254" layer="94"/>
<wire x1="33.02" y1="58.42" x2="33.02" y2="63.5" width="0.254" layer="94"/>
<wire x1="-25.4" y1="53.34" x2="-17.78" y2="53.34" width="0.254" layer="94"/>
<wire x1="-17.78" y1="53.34" x2="-17.78" y2="55.88" width="0.254" layer="94"/>
<wire x1="-17.78" y1="55.88" x2="-17.78" y2="58.42" width="0.254" layer="94"/>
<wire x1="-17.78" y1="58.42" x2="-25.4" y2="58.42" width="0.254" layer="94"/>
<wire x1="-17.78" y1="58.42" x2="-17.78" y2="60.96" width="0.254" layer="94"/>
<wire x1="-17.78" y1="60.96" x2="-17.78" y2="63.5" width="0.254" layer="94"/>
<wire x1="-17.78" y1="63.5" x2="-25.4" y2="63.5" width="0.254" layer="94"/>
<wire x1="33.02" y1="53.34" x2="25.4" y2="53.34" width="0.254" layer="94"/>
<wire x1="25.4" y1="53.34" x2="25.4" y2="55.88" width="0.254" layer="94"/>
<wire x1="25.4" y1="55.88" x2="25.4" y2="58.42" width="0.254" layer="94"/>
<wire x1="25.4" y1="58.42" x2="33.02" y2="58.42" width="0.254" layer="94"/>
<wire x1="25.4" y1="58.42" x2="25.4" y2="60.96" width="0.254" layer="94"/>
<wire x1="25.4" y1="60.96" x2="25.4" y2="63.5" width="0.254" layer="94"/>
<wire x1="25.4" y1="63.5" x2="33.02" y2="63.5" width="0.254" layer="94"/>
<wire x1="-25.4" y1="63.5" x2="-25.4" y2="68.58" width="0.254" layer="94"/>
<wire x1="-25.4" y1="68.58" x2="-25.4" y2="73.66" width="0.254" layer="94"/>
<wire x1="-25.4" y1="73.66" x2="-25.4" y2="78.74" width="0.254" layer="94"/>
<wire x1="33.02" y1="63.5" x2="33.02" y2="68.58" width="0.254" layer="94"/>
<wire x1="-25.4" y1="68.58" x2="-17.78" y2="68.58" width="0.254" layer="94"/>
<wire x1="-17.78" y1="68.58" x2="-17.78" y2="66.04" width="0.254" layer="94"/>
<wire x1="-17.78" y1="66.04" x2="-17.78" y2="63.5" width="0.254" layer="94"/>
<wire x1="-17.78" y1="66.04" x2="-12.7" y2="66.04" width="0.254" layer="94"/>
<wire x1="-12.7" y1="66.04" x2="-12.7" y2="63.5" width="0.254" layer="94"/>
<wire x1="-17.78" y1="60.96" x2="-15.24" y2="60.96" width="0.254" layer="94"/>
<wire x1="-17.78" y1="55.88" x2="-12.7" y2="55.88" width="0.254" layer="94"/>
<wire x1="-12.7" y1="55.88" x2="-12.7" y2="58.42" width="0.254" layer="94"/>
<wire x1="25.4" y1="63.5" x2="25.4" y2="66.04" width="0.254" layer="94"/>
<wire x1="25.4" y1="66.04" x2="25.4" y2="68.58" width="0.254" layer="94"/>
<wire x1="25.4" y1="68.58" x2="33.02" y2="68.58" width="0.254" layer="94"/>
<wire x1="25.4" y1="66.04" x2="20.32" y2="66.04" width="0.254" layer="94"/>
<wire x1="20.32" y1="66.04" x2="20.32" y2="63.5" width="0.254" layer="94"/>
<wire x1="25.4" y1="60.96" x2="22.86" y2="60.96" width="0.254" layer="94"/>
<wire x1="25.4" y1="55.88" x2="20.32" y2="55.88" width="0.254" layer="94"/>
<wire x1="20.32" y1="55.88" x2="20.32" y2="58.42" width="0.254" layer="94"/>
<wire x1="-25.4" y1="73.66" x2="-17.78" y2="73.66" width="0.254" layer="94"/>
<wire x1="-17.78" y1="73.66" x2="-17.78" y2="76.2" width="0.254" layer="94"/>
<wire x1="-17.78" y1="76.2" x2="-17.78" y2="78.74" width="0.254" layer="94"/>
<wire x1="-17.78" y1="78.74" x2="-25.4" y2="78.74" width="0.254" layer="94"/>
<wire x1="-25.4" y1="78.74" x2="-25.4" y2="83.82" width="0.254" layer="94"/>
<wire x1="-25.4" y1="83.82" x2="-17.78" y2="83.82" width="0.254" layer="94"/>
<wire x1="-17.78" y1="83.82" x2="-17.78" y2="81.28" width="0.254" layer="94"/>
<wire x1="-17.78" y1="81.28" x2="-17.78" y2="78.74" width="0.254" layer="94"/>
<wire x1="-25.4" y1="83.82" x2="-25.4" y2="88.9" width="0.254" layer="94"/>
<wire x1="-25.4" y1="88.9" x2="-25.4" y2="93.98" width="0.254" layer="94"/>
<wire x1="-25.4" y1="93.98" x2="-25.4" y2="99.06" width="0.254" layer="94"/>
<wire x1="-25.4" y1="99.06" x2="-25.4" y2="104.14" width="0.254" layer="94"/>
<wire x1="-25.4" y1="104.14" x2="-25.4" y2="109.22" width="0.254" layer="94"/>
<wire x1="33.02" y1="68.58" x2="33.02" y2="73.66" width="0.254" layer="94"/>
<wire x1="33.02" y1="73.66" x2="33.02" y2="78.74" width="0.254" layer="94"/>
<wire x1="33.02" y1="78.74" x2="33.02" y2="83.82" width="0.254" layer="94"/>
<wire x1="33.02" y1="83.82" x2="33.02" y2="88.9" width="0.254" layer="94"/>
<wire x1="33.02" y1="88.9" x2="33.02" y2="93.98" width="0.254" layer="94"/>
<wire x1="33.02" y1="93.98" x2="33.02" y2="99.06" width="0.254" layer="94"/>
<wire x1="33.02" y1="99.06" x2="33.02" y2="104.14" width="0.254" layer="94"/>
<wire x1="33.02" y1="104.14" x2="33.02" y2="109.22" width="0.254" layer="94"/>
<wire x1="-25.4" y1="88.9" x2="-17.78" y2="88.9" width="0.254" layer="94"/>
<wire x1="-17.78" y1="88.9" x2="-17.78" y2="86.36" width="0.254" layer="94"/>
<wire x1="-17.78" y1="86.36" x2="-17.78" y2="83.82" width="0.254" layer="94"/>
<wire x1="-17.78" y1="86.36" x2="-12.7" y2="86.36" width="0.254" layer="94"/>
<wire x1="-12.7" y1="86.36" x2="-12.7" y2="83.82" width="0.254" layer="94"/>
<wire x1="-17.78" y1="81.28" x2="-15.24" y2="81.28" width="0.254" layer="94"/>
<wire x1="-17.78" y1="76.2" x2="-12.7" y2="76.2" width="0.254" layer="94"/>
<wire x1="-12.7" y1="76.2" x2="-12.7" y2="78.74" width="0.254" layer="94"/>
<wire x1="33.02" y1="73.66" x2="25.4" y2="73.66" width="0.254" layer="94"/>
<wire x1="25.4" y1="73.66" x2="25.4" y2="76.2" width="0.254" layer="94"/>
<wire x1="25.4" y1="76.2" x2="25.4" y2="78.74" width="0.254" layer="94"/>
<wire x1="25.4" y1="78.74" x2="33.02" y2="78.74" width="0.254" layer="94"/>
<wire x1="25.4" y1="78.74" x2="25.4" y2="81.28" width="0.254" layer="94"/>
<wire x1="25.4" y1="81.28" x2="25.4" y2="83.82" width="0.254" layer="94"/>
<wire x1="25.4" y1="83.82" x2="33.02" y2="83.82" width="0.254" layer="94"/>
<wire x1="25.4" y1="83.82" x2="25.4" y2="86.36" width="0.254" layer="94"/>
<wire x1="25.4" y1="86.36" x2="25.4" y2="88.9" width="0.254" layer="94"/>
<wire x1="25.4" y1="88.9" x2="33.02" y2="88.9" width="0.254" layer="94"/>
<wire x1="25.4" y1="86.36" x2="20.32" y2="86.36" width="0.254" layer="94"/>
<wire x1="20.32" y1="86.36" x2="20.32" y2="83.82" width="0.254" layer="94"/>
<wire x1="25.4" y1="81.28" x2="22.86" y2="81.28" width="0.254" layer="94"/>
<wire x1="25.4" y1="76.2" x2="20.32" y2="76.2" width="0.254" layer="94"/>
<wire x1="20.32" y1="76.2" x2="20.32" y2="78.74" width="0.254" layer="94"/>
<wire x1="-25.4" y1="93.98" x2="-17.78" y2="93.98" width="0.254" layer="94"/>
<wire x1="-17.78" y1="93.98" x2="-17.78" y2="96.52" width="0.254" layer="94"/>
<wire x1="-17.78" y1="96.52" x2="-17.78" y2="99.06" width="0.254" layer="94"/>
<wire x1="-17.78" y1="99.06" x2="-25.4" y2="99.06" width="0.254" layer="94"/>
<wire x1="-17.78" y1="99.06" x2="-17.78" y2="101.6" width="0.254" layer="94"/>
<wire x1="-17.78" y1="101.6" x2="-17.78" y2="104.14" width="0.254" layer="94"/>
<wire x1="-17.78" y1="104.14" x2="-25.4" y2="104.14" width="0.254" layer="94"/>
<wire x1="-25.4" y1="109.22" x2="-17.78" y2="109.22" width="0.254" layer="94"/>
<wire x1="-17.78" y1="109.22" x2="-17.78" y2="106.68" width="0.254" layer="94"/>
<wire x1="-17.78" y1="104.14" x2="-17.78" y2="106.68" width="0.254" layer="94"/>
<wire x1="-17.78" y1="106.68" x2="-12.7" y2="106.68" width="0.254" layer="94"/>
<wire x1="-12.7" y1="106.68" x2="-12.7" y2="104.14" width="0.254" layer="94"/>
<wire x1="-17.78" y1="101.6" x2="-15.24" y2="101.6" width="0.254" layer="94"/>
<wire x1="-17.78" y1="96.52" x2="-12.7" y2="96.52" width="0.254" layer="94"/>
<wire x1="-12.7" y1="96.52" x2="-12.7" y2="99.06" width="0.254" layer="94"/>
<wire x1="33.02" y1="93.98" x2="25.4" y2="93.98" width="0.254" layer="94"/>
<wire x1="25.4" y1="93.98" x2="25.4" y2="96.52" width="0.254" layer="94"/>
<wire x1="25.4" y1="96.52" x2="25.4" y2="99.06" width="0.254" layer="94"/>
<wire x1="25.4" y1="99.06" x2="33.02" y2="99.06" width="0.254" layer="94"/>
<wire x1="25.4" y1="99.06" x2="25.4" y2="101.6" width="0.254" layer="94"/>
<wire x1="25.4" y1="101.6" x2="25.4" y2="104.14" width="0.254" layer="94"/>
<wire x1="33.02" y1="104.14" x2="25.4" y2="104.14" width="0.254" layer="94"/>
<wire x1="25.4" y1="104.14" x2="25.4" y2="106.68" width="0.254" layer="94"/>
<wire x1="25.4" y1="106.68" x2="25.4" y2="109.22" width="0.254" layer="94"/>
<wire x1="25.4" y1="109.22" x2="33.02" y2="109.22" width="0.254" layer="94"/>
<wire x1="25.4" y1="106.68" x2="20.32" y2="106.68" width="0.254" layer="94"/>
<wire x1="20.32" y1="106.68" x2="20.32" y2="104.14" width="0.254" layer="94"/>
<wire x1="25.4" y1="101.6" x2="22.86" y2="101.6" width="0.254" layer="94"/>
<wire x1="25.4" y1="96.52" x2="20.32" y2="96.52" width="0.254" layer="94"/>
<wire x1="20.32" y1="96.52" x2="20.32" y2="99.06" width="0.254" layer="94"/>
<wire x1="-25.4" y1="109.22" x2="-25.4" y2="114.3" width="0.254" layer="94"/>
<wire x1="-25.4" y1="114.3" x2="-25.4" y2="119.38" width="0.254" layer="94"/>
<wire x1="-25.4" y1="119.38" x2="-25.4" y2="124.46" width="0.254" layer="94"/>
<wire x1="33.02" y1="109.22" x2="33.02" y2="114.3" width="0.254" layer="94"/>
<wire x1="33.02" y1="114.3" x2="33.02" y2="119.38" width="0.254" layer="94"/>
<wire x1="33.02" y1="119.38" x2="33.02" y2="124.46" width="0.254" layer="94"/>
<wire x1="33.02" y1="124.46" x2="33.02" y2="129.54" width="0.254" layer="94"/>
<wire x1="33.02" y1="129.54" x2="33.02" y2="134.62" width="0.254" layer="94"/>
<wire x1="33.02" y1="134.62" x2="33.02" y2="139.7" width="0.254" layer="94"/>
<wire x1="-25.4" y1="114.3" x2="-17.78" y2="114.3" width="0.254" layer="94"/>
<wire x1="-17.78" y1="114.3" x2="-17.78" y2="116.84" width="0.254" layer="94"/>
<wire x1="-17.78" y1="116.84" x2="-17.78" y2="119.38" width="0.254" layer="94"/>
<wire x1="-25.4" y1="119.38" x2="-17.78" y2="119.38" width="0.254" layer="94"/>
<wire x1="-17.78" y1="119.38" x2="-17.78" y2="121.92" width="0.254" layer="94"/>
<wire x1="-17.78" y1="121.92" x2="-17.78" y2="124.46" width="0.254" layer="94"/>
<wire x1="-17.78" y1="124.46" x2="-25.4" y2="124.46" width="0.254" layer="94"/>
<wire x1="-25.4" y1="124.46" x2="-25.4" y2="129.54" width="0.254" layer="94"/>
<wire x1="-25.4" y1="129.54" x2="-25.4" y2="134.62" width="0.254" layer="94"/>
<wire x1="-25.4" y1="134.62" x2="-25.4" y2="139.7" width="0.254" layer="94"/>
<wire x1="-25.4" y1="139.7" x2="-25.4" y2="144.78" width="0.254" layer="94"/>
<wire x1="-25.4" y1="129.54" x2="-17.78" y2="129.54" width="0.254" layer="94"/>
<wire x1="-17.78" y1="129.54" x2="-17.78" y2="127" width="0.254" layer="94"/>
<wire x1="-17.78" y1="124.46" x2="-17.78" y2="127" width="0.254" layer="94"/>
<wire x1="-17.78" y1="127" x2="-12.7" y2="127" width="0.254" layer="94"/>
<wire x1="-12.7" y1="127" x2="-12.7" y2="124.46" width="0.254" layer="94"/>
<wire x1="-17.78" y1="121.92" x2="-15.24" y2="121.92" width="0.254" layer="94"/>
<wire x1="-17.78" y1="116.84" x2="-12.7" y2="116.84" width="0.254" layer="94"/>
<wire x1="-12.7" y1="116.84" x2="-12.7" y2="119.38" width="0.254" layer="94"/>
<wire x1="33.02" y1="114.3" x2="25.4" y2="114.3" width="0.254" layer="94"/>
<wire x1="25.4" y1="114.3" x2="25.4" y2="116.84" width="0.254" layer="94"/>
<wire x1="25.4" y1="116.84" x2="25.4" y2="119.38" width="0.254" layer="94"/>
<wire x1="33.02" y1="119.38" x2="25.4" y2="119.38" width="0.254" layer="94"/>
<wire x1="25.4" y1="119.38" x2="25.4" y2="121.92" width="0.254" layer="94"/>
<wire x1="25.4" y1="121.92" x2="25.4" y2="124.46" width="0.254" layer="94"/>
<wire x1="33.02" y1="124.46" x2="25.4" y2="124.46" width="0.254" layer="94"/>
<wire x1="25.4" y1="124.46" x2="25.4" y2="127" width="0.254" layer="94"/>
<wire x1="25.4" y1="127" x2="25.4" y2="129.54" width="0.254" layer="94"/>
<wire x1="25.4" y1="129.54" x2="33.02" y2="129.54" width="0.254" layer="94"/>
<wire x1="25.4" y1="127" x2="20.32" y2="127" width="0.254" layer="94"/>
<wire x1="20.32" y1="127" x2="20.32" y2="124.46" width="0.254" layer="94"/>
<wire x1="25.4" y1="121.92" x2="22.86" y2="121.92" width="0.254" layer="94"/>
<wire x1="25.4" y1="116.84" x2="20.32" y2="116.84" width="0.254" layer="94"/>
<wire x1="20.32" y1="116.84" x2="20.32" y2="119.38" width="0.254" layer="94"/>
<wire x1="-25.4" y1="134.62" x2="-17.78" y2="134.62" width="0.254" layer="94"/>
<wire x1="-17.78" y1="134.62" x2="-17.78" y2="137.16" width="0.254" layer="94"/>
<wire x1="-17.78" y1="137.16" x2="-17.78" y2="139.7" width="0.254" layer="94"/>
<wire x1="-17.78" y1="139.7" x2="-25.4" y2="139.7" width="0.254" layer="94"/>
<wire x1="-17.78" y1="139.7" x2="-17.78" y2="142.24" width="0.254" layer="94"/>
<wire x1="-17.78" y1="142.24" x2="-17.78" y2="144.78" width="0.254" layer="94"/>
<wire x1="-17.78" y1="144.78" x2="-25.4" y2="144.78" width="0.254" layer="94"/>
<wire x1="-17.78" y1="144.78" x2="-17.78" y2="147.32" width="0.254" layer="94"/>
<wire x1="-17.78" y1="147.32" x2="-17.78" y2="149.86" width="0.254" layer="94"/>
<wire x1="-17.78" y1="149.86" x2="-25.4" y2="149.86" width="0.254" layer="94"/>
<wire x1="-25.4" y1="149.86" x2="-25.4" y2="144.78" width="0.254" layer="94"/>
<wire x1="-17.78" y1="147.32" x2="-12.7" y2="147.32" width="0.254" layer="94"/>
<wire x1="-12.7" y1="147.32" x2="-12.7" y2="144.78" width="0.254" layer="94"/>
<wire x1="-17.78" y1="142.24" x2="-15.24" y2="142.24" width="0.254" layer="94"/>
<wire x1="-17.78" y1="137.16" x2="-12.7" y2="137.16" width="0.254" layer="94"/>
<wire x1="-12.7" y1="137.16" x2="-12.7" y2="139.7" width="0.254" layer="94"/>
<wire x1="33.02" y1="134.62" x2="25.4" y2="134.62" width="0.254" layer="94"/>
<wire x1="25.4" y1="134.62" x2="25.4" y2="137.16" width="0.254" layer="94"/>
<wire x1="25.4" y1="137.16" x2="25.4" y2="139.7" width="0.254" layer="94"/>
<wire x1="25.4" y1="139.7" x2="33.02" y2="139.7" width="0.254" layer="94"/>
<wire x1="33.02" y1="139.7" x2="33.02" y2="144.78" width="0.254" layer="94"/>
<wire x1="33.02" y1="144.78" x2="33.02" y2="149.86" width="0.254" layer="94"/>
<wire x1="25.4" y1="139.7" x2="25.4" y2="142.24" width="0.254" layer="94"/>
<wire x1="25.4" y1="142.24" x2="25.4" y2="144.78" width="0.254" layer="94"/>
<wire x1="25.4" y1="144.78" x2="33.02" y2="144.78" width="0.254" layer="94"/>
<wire x1="25.4" y1="144.78" x2="25.4" y2="147.32" width="0.254" layer="94"/>
<wire x1="25.4" y1="147.32" x2="25.4" y2="149.86" width="0.254" layer="94"/>
<wire x1="25.4" y1="149.86" x2="33.02" y2="149.86" width="0.254" layer="94"/>
<wire x1="25.4" y1="147.32" x2="20.32" y2="147.32" width="0.254" layer="94"/>
<wire x1="20.32" y1="147.32" x2="20.32" y2="144.78" width="0.254" layer="94"/>
<wire x1="25.4" y1="142.24" x2="22.86" y2="142.24" width="0.254" layer="94"/>
<wire x1="25.4" y1="137.16" x2="20.32" y2="137.16" width="0.254" layer="94"/>
<wire x1="20.32" y1="137.16" x2="20.32" y2="139.7" width="0.254" layer="94"/>
<wire x1="-25.4" y1="149.86" x2="-25.4" y2="154.94" width="0.254" layer="94"/>
<wire x1="-25.4" y1="154.94" x2="-25.4" y2="160.02" width="0.254" layer="94"/>
<wire x1="-25.4" y1="160.02" x2="-25.4" y2="165.1" width="0.254" layer="94"/>
<wire x1="-25.4" y1="165.1" x2="-25.4" y2="170.18" width="0.254" layer="94"/>
<wire x1="-25.4" y1="170.18" x2="-25.4" y2="175.26" width="0.254" layer="94"/>
<wire x1="-25.4" y1="154.94" x2="-17.78" y2="154.94" width="0.254" layer="94"/>
<wire x1="-17.78" y1="154.94" x2="-17.78" y2="157.48" width="0.254" layer="94"/>
<wire x1="-17.78" y1="157.48" x2="-17.78" y2="160.02" width="0.254" layer="94"/>
<wire x1="-25.4" y1="160.02" x2="-17.78" y2="160.02" width="0.254" layer="94"/>
<wire x1="-17.78" y1="160.02" x2="-17.78" y2="162.56" width="0.254" layer="94"/>
<wire x1="-17.78" y1="162.56" x2="-17.78" y2="165.1" width="0.254" layer="94"/>
<wire x1="-25.4" y1="165.1" x2="-17.78" y2="165.1" width="0.254" layer="94"/>
<wire x1="-17.78" y1="165.1" x2="-17.78" y2="167.64" width="0.254" layer="94"/>
<wire x1="-25.4" y1="170.18" x2="-17.78" y2="170.18" width="0.254" layer="94"/>
<wire x1="-17.78" y1="170.18" x2="-17.78" y2="167.64" width="0.254" layer="94"/>
<wire x1="-17.78" y1="167.64" x2="-12.7" y2="167.64" width="0.254" layer="94"/>
<wire x1="-12.7" y1="167.64" x2="-12.7" y2="165.1" width="0.254" layer="94"/>
<wire x1="-17.78" y1="162.56" x2="-15.24" y2="162.56" width="0.254" layer="94"/>
<wire x1="-17.78" y1="157.48" x2="-12.7" y2="157.48" width="0.254" layer="94"/>
<wire x1="-12.7" y1="157.48" x2="-12.7" y2="160.02" width="0.254" layer="94"/>
<wire x1="33.02" y1="149.86" x2="33.02" y2="154.94" width="0.254" layer="94"/>
<wire x1="33.02" y1="154.94" x2="33.02" y2="160.02" width="0.254" layer="94"/>
<wire x1="33.02" y1="160.02" x2="33.02" y2="165.1" width="0.254" layer="94"/>
<wire x1="33.02" y1="165.1" x2="33.02" y2="170.18" width="0.254" layer="94"/>
<wire x1="33.02" y1="170.18" x2="33.02" y2="175.26" width="0.254" layer="94"/>
<wire x1="33.02" y1="154.94" x2="25.4" y2="154.94" width="0.254" layer="94"/>
<wire x1="25.4" y1="154.94" x2="25.4" y2="157.48" width="0.254" layer="94"/>
<wire x1="25.4" y1="157.48" x2="25.4" y2="160.02" width="0.254" layer="94"/>
<wire x1="33.02" y1="160.02" x2="25.4" y2="160.02" width="0.254" layer="94"/>
<wire x1="25.4" y1="160.02" x2="25.4" y2="162.56" width="0.254" layer="94"/>
<wire x1="25.4" y1="162.56" x2="25.4" y2="165.1" width="0.254" layer="94"/>
<wire x1="33.02" y1="165.1" x2="25.4" y2="165.1" width="0.254" layer="94"/>
<wire x1="25.4" y1="165.1" x2="25.4" y2="167.64" width="0.254" layer="94"/>
<wire x1="25.4" y1="167.64" x2="25.4" y2="170.18" width="0.254" layer="94"/>
<wire x1="25.4" y1="170.18" x2="33.02" y2="170.18" width="0.254" layer="94"/>
<wire x1="25.4" y1="167.64" x2="20.32" y2="167.64" width="0.254" layer="94"/>
<wire x1="20.32" y1="167.64" x2="20.32" y2="165.1" width="0.254" layer="94"/>
<wire x1="25.4" y1="162.56" x2="22.86" y2="162.56" width="0.254" layer="94"/>
<wire x1="25.4" y1="157.48" x2="20.32" y2="157.48" width="0.254" layer="94"/>
<wire x1="20.32" y1="157.48" x2="20.32" y2="160.02" width="0.254" layer="94"/>
<wire x1="-25.4" y1="175.26" x2="33.02" y2="175.26" width="0.254" layer="94"/>
<pin name="VCC" x="0" y="180.34" length="middle" rot="R270"/>
<pin name="GND" x="5.08" y="180.34" length="middle" rot="R270"/>
<wire x1="-22.86" y1="25.4" x2="-20.32" y2="25.4" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="25.4" x2="-20.32" y2="25.4" width="0.254" layer="94" curve="-180"/>
<wire x1="-20.32" y1="25.4" x2="-22.86" y2="25.4" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="20.32" x2="-20.32" y2="20.32" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="20.32" x2="-20.32" y2="20.32" width="0.254" layer="94" curve="-180"/>
<wire x1="-20.32" y1="20.32" x2="-22.86" y2="20.32" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="15.24" x2="-20.32" y2="15.24" width="0.254" layer="94" curve="-180"/>
<wire x1="-20.32" y1="15.24" x2="-22.86" y2="15.24" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="25.4" x2="30.48" y2="25.4" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="25.4" x2="30.48" y2="25.4" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="25.4" x2="30.48" y2="25.4" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="25.4" x2="30.48" y2="25.4" width="0.254" layer="94" curve="-180"/>
<wire x1="30.48" y1="25.4" x2="27.94" y2="25.4" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="20.32" x2="30.48" y2="20.32" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="20.32" x2="30.48" y2="20.32" width="0.254" layer="94" curve="-180"/>
<wire x1="30.48" y1="20.32" x2="27.94" y2="20.32" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="15.24" x2="30.48" y2="15.24" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="15.24" x2="30.48" y2="15.24" width="0.254" layer="94" curve="-180"/>
<wire x1="30.48" y1="15.24" x2="27.94" y2="15.24" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="40.64" x2="30.48" y2="40.64" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="40.64" x2="30.48" y2="40.64" width="0.254" layer="94" curve="-180"/>
<wire x1="30.48" y1="40.64" x2="27.94" y2="40.64" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="35.56" x2="30.48" y2="35.56" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="35.56" x2="30.48" y2="35.56" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="35.56" x2="-20.32" y2="35.56" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="35.56" x2="-20.32" y2="35.56" width="0.254" layer="94" curve="-180"/>
<wire x1="-20.32" y1="35.56" x2="-22.86" y2="35.56" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="40.64" x2="-20.32" y2="40.64" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="40.64" x2="-20.32" y2="40.64" width="0.254" layer="94" curve="-180"/>
<wire x1="-20.32" y1="40.64" x2="-22.86" y2="40.64" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="45.72" x2="30.48" y2="45.72" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="35.56" x2="30.48" y2="35.56" width="0.254" layer="94" curve="-180"/>
<wire x1="30.48" y1="35.56" x2="27.94" y2="35.56" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="45.72" x2="30.48" y2="45.72" width="0.254" layer="94" curve="-180"/>
<wire x1="30.48" y1="45.72" x2="27.94" y2="45.72" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="45.72" x2="-20.32" y2="45.72" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="45.72" x2="-20.32" y2="45.72" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="45.72" x2="-20.32" y2="45.72" width="0.254" layer="94" curve="-180"/>
<wire x1="-20.32" y1="45.72" x2="-22.86" y2="45.72" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="55.88" x2="-20.32" y2="55.88" width="0.254" layer="94" curve="-180"/>
<wire x1="-20.32" y1="55.88" x2="-22.86" y2="55.88" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="55.88" x2="30.48" y2="55.88" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="55.88" x2="30.48" y2="55.88" width="0.254" layer="94" curve="-180"/>
<wire x1="30.48" y1="55.88" x2="27.94" y2="55.88" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="60.96" x2="30.48" y2="60.96" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="60.96" x2="30.48" y2="60.96" width="0.254" layer="94" curve="-180"/>
<wire x1="30.48" y1="60.96" x2="27.94" y2="60.96" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="66.04" x2="30.48" y2="66.04" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="66.04" x2="30.48" y2="66.04" width="0.254" layer="94" curve="-180"/>
<wire x1="30.48" y1="66.04" x2="27.94" y2="66.04" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="60.96" x2="-20.32" y2="60.96" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="60.96" x2="-20.32" y2="60.96" width="0.254" layer="94" curve="-180"/>
<wire x1="-20.32" y1="60.96" x2="-22.86" y2="60.96" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="66.04" x2="-20.32" y2="66.04" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="66.04" x2="-20.32" y2="66.04" width="0.254" layer="94" curve="-180"/>
<wire x1="-20.32" y1="66.04" x2="-22.86" y2="66.04" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="86.36" x2="-20.32" y2="86.36" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="86.36" x2="-20.32" y2="86.36" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="86.36" x2="-20.32" y2="86.36" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="86.36" x2="-20.32" y2="86.36" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="86.36" x2="-20.32" y2="86.36" width="0.254" layer="94" curve="-180"/>
<wire x1="-20.32" y1="86.36" x2="-22.86" y2="86.36" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="81.28" x2="-20.32" y2="81.28" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="81.28" x2="-20.32" y2="81.28" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="76.2" x2="-20.32" y2="76.2" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="76.2" x2="-20.32" y2="76.2" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="76.2" x2="-20.32" y2="76.2" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="81.28" x2="-20.32" y2="81.28" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="86.36" x2="30.48" y2="86.36" width="0.254" layer="94" curve="-180"/>
<wire x1="30.48" y1="86.36" x2="27.94" y2="86.36" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="81.28" x2="-20.32" y2="81.28" width="0.254" layer="94" curve="-180"/>
<wire x1="-20.32" y1="81.28" x2="-22.86" y2="81.28" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="76.2" x2="-20.32" y2="76.2" width="0.254" layer="94" curve="-180"/>
<wire x1="-20.32" y1="76.2" x2="-22.86" y2="76.2" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="81.28" x2="30.48" y2="81.28" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="81.28" x2="30.48" y2="81.28" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="81.28" x2="30.48" y2="81.28" width="0.254" layer="94" curve="-180"/>
<wire x1="30.48" y1="81.28" x2="27.94" y2="81.28" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="76.2" x2="30.48" y2="76.2" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="101.6" x2="30.48" y2="101.6" width="0.254" layer="94" curve="-180"/>
<wire x1="30.48" y1="101.6" x2="27.94" y2="101.6" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="96.52" x2="30.48" y2="96.52" width="0.254" layer="94" curve="-180"/>
<wire x1="30.48" y1="96.52" x2="27.94" y2="96.52" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="96.52" x2="-20.32" y2="96.52" width="0.254" layer="94" curve="-180"/>
<wire x1="-20.32" y1="96.52" x2="-22.86" y2="96.52" width="0.254" layer="94" curve="-180"/>
<wire x1="30.48" y1="76.2" x2="27.94" y2="76.2" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="101.6" x2="-20.32" y2="101.6" width="0.254" layer="94" curve="-180"/>
<wire x1="-20.32" y1="101.6" x2="-22.86" y2="101.6" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="106.68" x2="-20.32" y2="106.68" width="0.254" layer="94" curve="-180"/>
<wire x1="-20.32" y1="106.68" x2="-22.86" y2="106.68" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="106.68" x2="30.48" y2="106.68" width="0.254" layer="94" curve="-180"/>
<wire x1="30.48" y1="106.68" x2="27.94" y2="106.68" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="127" x2="-20.32" y2="127" width="0.254" layer="94" curve="-180"/>
<wire x1="-20.32" y1="127" x2="-22.86" y2="127" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="121.92" x2="-20.32" y2="121.92" width="0.254" layer="94" curve="-180"/>
<wire x1="-20.32" y1="121.92" x2="-22.86" y2="121.92" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="116.84" x2="-20.32" y2="116.84" width="0.254" layer="94" curve="-180"/>
<wire x1="-20.32" y1="116.84" x2="-22.86" y2="116.84" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="127" x2="30.48" y2="127" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="127" x2="30.48" y2="127" width="0.254" layer="94" curve="-180"/>
<wire x1="30.48" y1="127" x2="27.94" y2="127" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="121.92" x2="30.48" y2="121.92" width="0.254" layer="94" curve="-180"/>
<wire x1="30.48" y1="121.92" x2="27.94" y2="121.92" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="116.84" x2="30.48" y2="116.84" width="0.254" layer="94" curve="-180"/>
<wire x1="30.48" y1="116.84" x2="27.94" y2="116.84" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="137.16" x2="30.48" y2="137.16" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="137.16" x2="30.48" y2="137.16" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="137.16" x2="30.48" y2="137.16" width="0.254" layer="94" curve="-180"/>
<wire x1="30.48" y1="137.16" x2="27.94" y2="137.16" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="142.24" x2="30.48" y2="142.24" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="142.24" x2="30.48" y2="142.24" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="142.24" x2="30.48" y2="142.24" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="142.24" x2="30.48" y2="142.24" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="142.24" x2="30.48" y2="142.24" width="0.254" layer="94" curve="-180"/>
<wire x1="30.48" y1="142.24" x2="27.94" y2="142.24" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="142.24" x2="-20.32" y2="142.24" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="142.24" x2="-20.32" y2="142.24" width="0.254" layer="94" curve="-180"/>
<wire x1="-20.32" y1="142.24" x2="-22.86" y2="142.24" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="137.16" x2="-20.32" y2="137.16" width="0.254" layer="94" curve="-180"/>
<wire x1="-20.32" y1="137.16" x2="-22.86" y2="137.16" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="147.32" x2="-20.32" y2="147.32" width="0.254" layer="94" curve="-180"/>
<wire x1="-20.32" y1="147.32" x2="-22.86" y2="147.32" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="147.32" x2="30.48" y2="147.32" width="0.254" layer="94" curve="-180"/>
<wire x1="30.48" y1="147.32" x2="27.94" y2="147.32" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="167.64" x2="30.48" y2="167.64" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="167.64" x2="30.48" y2="167.64" width="0.254" layer="94" curve="-180"/>
<wire x1="30.48" y1="167.64" x2="27.94" y2="167.64" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="162.56" x2="30.48" y2="162.56" width="0.254" layer="94" curve="-180"/>
<wire x1="30.48" y1="162.56" x2="27.94" y2="162.56" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="157.48" x2="30.48" y2="157.48" width="0.254" layer="94" curve="-180"/>
<wire x1="27.94" y1="157.48" x2="30.48" y2="157.48" width="0.254" layer="94" curve="-180"/>
<wire x1="30.48" y1="157.48" x2="27.94" y2="157.48" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="157.48" x2="-20.32" y2="157.48" width="0.254" layer="94" curve="-180"/>
<wire x1="-20.32" y1="157.48" x2="-22.86" y2="157.48" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="162.56" x2="-20.32" y2="162.56" width="0.254" layer="94" curve="-180"/>
<wire x1="-20.32" y1="162.56" x2="-22.86" y2="162.56" width="0.254" layer="94" curve="-180"/>
<wire x1="-22.86" y1="167.64" x2="-20.32" y2="167.64" width="0.254" layer="94" curve="-180"/>
<wire x1="-20.32" y1="167.64" x2="-22.86" y2="167.64" width="0.254" layer="94" curve="-180"/>
<text x="-10.16" y="20.32" size="1.778" layer="94">K8</text>
<text x="-10.16" y="40.64" size="1.778" layer="94">K7</text>
<text x="-10.16" y="60.96" size="1.778" layer="94">K6</text>
<text x="-10.16" y="81.28" size="1.778" layer="94">K5</text>
<text x="15.24" y="20.32" size="1.778" layer="94">K16</text>
<text x="15.24" y="40.64" size="1.778" layer="94">K15</text>
<text x="15.24" y="60.96" size="1.778" layer="94">K14</text>
<text x="15.24" y="81.28" size="1.778" layer="94">K13</text>
<text x="15.24" y="101.6" size="1.778" layer="94">K12</text>
<text x="-10.16" y="101.6" size="1.778" layer="94">K4</text>
<text x="-10.16" y="121.92" size="1.778" layer="94">K3</text>
<text x="15.24" y="121.92" size="1.778" layer="94">K11</text>
<text x="-10.16" y="142.24" size="1.778" layer="94">K2</text>
<text x="-10.16" y="162.56" size="1.778" layer="94">K1</text>
<text x="15.24" y="142.24" size="1.778" layer="94">K10</text>
<text x="15.24" y="162.56" size="1.778" layer="94">K9</text>
<pin name="NC.K1" x="-30.48" y="15.24" length="middle"/>
<pin name="COM.K1" x="-30.48" y="20.32" length="middle"/>
<pin name="NC.K2" x="-30.48" y="35.56" length="middle"/>
<pin name="COM.K2" x="-30.48" y="40.64" length="middle"/>
<pin name="NC.K3" x="-30.48" y="55.88" length="middle"/>
<pin name="COM.K3" x="-30.48" y="60.96" length="middle"/>
<pin name="NC.K4" x="-30.48" y="76.2" length="middle"/>
<pin name="COM.K4" x="-30.48" y="81.28" length="middle"/>
<pin name="NC.K5" x="-30.48" y="96.52" length="middle"/>
<pin name="COM.K5" x="-30.48" y="101.6" length="middle"/>
<pin name="NC.K6" x="-30.48" y="116.84" length="middle"/>
<pin name="COM.K6" x="-30.48" y="121.92" length="middle"/>
<pin name="NC.K7" x="-30.48" y="137.16" length="middle"/>
<pin name="COM.K7" x="-30.48" y="142.24" length="middle"/>
<pin name="COM.K15" x="38.1" y="142.24" length="middle" rot="R180"/>
<pin name="NC.K14" x="38.1" y="127" length="middle" rot="R180"/>
<pin name="COM.K14" x="38.1" y="121.92" length="middle" rot="R180"/>
<pin name="NC.K15" x="38.1" y="147.32" length="middle" rot="R180"/>
<pin name="NC.K13" x="38.1" y="106.68" length="middle" rot="R180"/>
<pin name="COM.K13" x="38.1" y="101.6" length="middle" rot="R180"/>
<pin name="NC.K12" x="38.1" y="86.36" length="middle" rot="R180"/>
<pin name="COM.K12" x="38.1" y="81.28" length="middle" rot="R180"/>
<pin name="NC.K11" x="38.1" y="66.04" length="middle" rot="R180"/>
<pin name="COM.K11" x="38.1" y="60.96" length="middle" rot="R180"/>
<pin name="NC.K10" x="38.1" y="45.72" length="middle" rot="R180"/>
<pin name="COM.K10" x="38.1" y="40.64" length="middle" rot="R180"/>
<pin name="NC.K9" x="38.1" y="25.4" length="middle" rot="R180"/>
<pin name="COM.K9" x="38.1" y="20.32" length="middle" rot="R180"/>
<pin name="NC.K8" x="-30.48" y="157.48" length="middle"/>
<pin name="COM.K8" x="-30.48" y="162.56" length="middle"/>
<pin name="COM.K16" x="38.1" y="162.56" length="middle" rot="R180"/>
<pin name="NC.K16" x="38.1" y="167.64" length="middle" rot="R180"/>
<text x="30.48" y="177.8" size="1.778" layer="94" rot="R90">RELAY 16 CH</text>
<pin name="NO.K1" x="-30.48" y="25.4" length="middle"/>
<pin name="NO.K2" x="-30.48" y="45.72" length="middle"/>
<pin name="NO.K3" x="-30.48" y="66.04" length="middle"/>
<pin name="NO.K4" x="-30.48" y="86.36" length="middle"/>
<pin name="NO.K5" x="-30.48" y="106.68" length="middle"/>
<pin name="NO.K6" x="-30.48" y="127" length="middle"/>
<pin name="NO.K7" x="-30.48" y="147.32" length="middle"/>
<pin name="NO.K8" x="-30.48" y="167.64" length="middle"/>
<pin name="NO.K16" x="38.1" y="157.48" length="middle" rot="R180"/>
<pin name="NO.K15" x="38.1" y="137.16" length="middle" rot="R180"/>
<pin name="NO.K14" x="38.1" y="116.84" length="middle" rot="R180"/>
<pin name="NO.K13" x="38.1" y="96.52" length="middle" rot="R180"/>
<pin name="NO.K12" x="38.1" y="76.2" length="middle" rot="R180"/>
<pin name="NO.K11" x="38.1" y="55.88" length="middle" rot="R180"/>
<pin name="NO.K10" x="38.1" y="35.56" length="middle" rot="R180"/>
<pin name="NO.K9" x="38.1" y="15.24" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RELAY16CHANNEL">
<gates>
<gate name="G$1" symbol="RELAY16CHANNEL" x="-7.62" y="-35.56"/>
</gates>
<devices>
<device name="" package="RELAY16">
<connects>
<connect gate="G$1" pin="COM.K1" pad="22"/>
<connect gate="G$1" pin="COM.K10" pad="49"/>
<connect gate="G$1" pin="COM.K11" pad="52"/>
<connect gate="G$1" pin="COM.K12" pad="55"/>
<connect gate="G$1" pin="COM.K13" pad="58"/>
<connect gate="G$1" pin="COM.K14" pad="61"/>
<connect gate="G$1" pin="COM.K15" pad="64"/>
<connect gate="G$1" pin="COM.K16" pad="67"/>
<connect gate="G$1" pin="COM.K2" pad="25"/>
<connect gate="G$1" pin="COM.K3" pad="28"/>
<connect gate="G$1" pin="COM.K4" pad="31"/>
<connect gate="G$1" pin="COM.K5" pad="34"/>
<connect gate="G$1" pin="COM.K6" pad="37"/>
<connect gate="G$1" pin="COM.K7" pad="40"/>
<connect gate="G$1" pin="COM.K8" pad="43"/>
<connect gate="G$1" pin="COM.K9" pad="46"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GND.1" pad="19"/>
<connect gate="G$1" pin="GND.2" pad="20"/>
<connect gate="G$1" pin="IN1" pad="3"/>
<connect gate="G$1" pin="IN10" pad="12"/>
<connect gate="G$1" pin="IN11" pad="13"/>
<connect gate="G$1" pin="IN12" pad="14"/>
<connect gate="G$1" pin="IN13" pad="15"/>
<connect gate="G$1" pin="IN14" pad="16"/>
<connect gate="G$1" pin="IN15" pad="17"/>
<connect gate="G$1" pin="IN16" pad="18"/>
<connect gate="G$1" pin="IN2" pad="4"/>
<connect gate="G$1" pin="IN3" pad="5"/>
<connect gate="G$1" pin="IN4" pad="6"/>
<connect gate="G$1" pin="IN5" pad="7"/>
<connect gate="G$1" pin="IN6" pad="8"/>
<connect gate="G$1" pin="IN7" pad="9"/>
<connect gate="G$1" pin="IN8" pad="10"/>
<connect gate="G$1" pin="IN9" pad="11"/>
<connect gate="G$1" pin="NC.K1" pad="21"/>
<connect gate="G$1" pin="NC.K10" pad="48"/>
<connect gate="G$1" pin="NC.K11" pad="51"/>
<connect gate="G$1" pin="NC.K12" pad="54"/>
<connect gate="G$1" pin="NC.K13" pad="57"/>
<connect gate="G$1" pin="NC.K14" pad="60"/>
<connect gate="G$1" pin="NC.K15" pad="63"/>
<connect gate="G$1" pin="NC.K16" pad="66"/>
<connect gate="G$1" pin="NC.K2" pad="24"/>
<connect gate="G$1" pin="NC.K3" pad="27"/>
<connect gate="G$1" pin="NC.K4" pad="30"/>
<connect gate="G$1" pin="NC.K5" pad="33"/>
<connect gate="G$1" pin="NC.K6" pad="36"/>
<connect gate="G$1" pin="NC.K7" pad="39"/>
<connect gate="G$1" pin="NC.K8" pad="42"/>
<connect gate="G$1" pin="NC.K9" pad="45"/>
<connect gate="G$1" pin="NO.K1" pad="23"/>
<connect gate="G$1" pin="NO.K10" pad="50"/>
<connect gate="G$1" pin="NO.K11" pad="53"/>
<connect gate="G$1" pin="NO.K12" pad="56"/>
<connect gate="G$1" pin="NO.K13" pad="59"/>
<connect gate="G$1" pin="NO.K14" pad="62"/>
<connect gate="G$1" pin="NO.K15" pad="65"/>
<connect gate="G$1" pin="NO.K16" pad="68"/>
<connect gate="G$1" pin="NO.K2" pad="26"/>
<connect gate="G$1" pin="NO.K3" pad="29"/>
<connect gate="G$1" pin="NO.K4" pad="32"/>
<connect gate="G$1" pin="NO.K5" pad="35"/>
<connect gate="G$1" pin="NO.K6" pad="38"/>
<connect gate="G$1" pin="NO.K7" pad="41"/>
<connect gate="G$1" pin="NO.K8" pad="44"/>
<connect gate="G$1" pin="NO.K9" pad="47"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
<connect gate="G$1" pin="VCC.1" pad="1"/>
<connect gate="G$1" pin="VCC.2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="lokercontroller">
<packages>
<package name="LOKERCONTROLER">
<pad name="3" x="-6.35" y="-2.54" drill="0.6" shape="square"/>
<pad name="4" x="-3.81" y="-2.54" drill="0.6" shape="square"/>
<pad name="5" x="-2.54" y="-2.54" drill="0.6" shape="square"/>
<pad name="6" x="0" y="-2.54" drill="0.6" shape="square"/>
<pad name="1" x="-20.32" y="-2.54" drill="0.6" shape="square"/>
<pad name="2" x="-19.05" y="-2.54" drill="0.6" shape="square"/>
<pad name="7" x="-20.32" y="10.16" drill="0.6" shape="square"/>
<pad name="8" x="-17.78" y="10.16" drill="0.6" shape="square"/>
<pad name="9" x="-15.24" y="10.16" drill="0.6" shape="square"/>
<pad name="10" x="-13.97" y="10.16" drill="0.6" shape="square"/>
<pad name="11" x="-8.89" y="10.16" drill="0.6" shape="square"/>
<pad name="12" x="-6.35" y="10.16" drill="0.6" shape="square"/>
<pad name="13" x="-3.81" y="10.16" drill="0.6" shape="square"/>
<pad name="14" x="-1.27" y="10.16" drill="0.6" shape="square"/>
<pad name="15" x="3.81" y="10.16" drill="0.6" shape="square"/>
<pad name="16" x="7.62" y="10.16" drill="0.6" shape="square"/>
<pad name="17" x="8.89" y="10.16" drill="0.6" shape="square"/>
<pad name="18" x="11.43" y="10.16" drill="0.6" shape="square"/>
<pad name="19" x="16.51" y="10.16" drill="0.6" shape="square"/>
<pad name="20" x="20.32" y="10.16" drill="0.6" shape="square"/>
<pad name="21" x="21.59" y="10.16" drill="0.6" shape="square"/>
<pad name="22" x="24.13" y="10.16" drill="0.6" shape="square"/>
<pad name="23" x="-8.89" y="7.62" drill="0.6" shape="square"/>
<pad name="24" x="-7.62" y="7.62" drill="0.6" shape="square"/>
<pad name="25" x="-6.35" y="7.62" drill="0.6" shape="square"/>
<pad name="26" x="-5.08" y="7.62" drill="0.6" shape="square"/>
<pad name="27" x="-2.54" y="7.62" drill="0.6" shape="square"/>
<pad name="28" x="-1.27" y="7.62" drill="0.6" shape="square"/>
<pad name="29" x="0" y="7.62" drill="0.6" shape="square"/>
<pad name="30" x="1.27" y="7.62" drill="0.6" shape="square"/>
<pad name="31" x="3.81" y="7.62" drill="0.6" shape="square"/>
<pad name="32" x="5.08" y="7.62" drill="0.6" shape="square"/>
<pad name="33" x="6.35" y="7.62" drill="0.6" shape="square"/>
<pad name="34" x="7.62" y="7.62" drill="0.6" shape="square"/>
<pad name="35" x="10.16" y="7.62" drill="0.6" shape="square"/>
<pad name="37" x="12.7" y="7.62" drill="0.6" shape="square"/>
<pad name="36" x="11.43" y="7.62" drill="0.6" shape="square"/>
<pad name="38" x="13.97" y="7.62" drill="0.6" shape="square"/>
<pad name="39" x="17.78" y="7.62" drill="0.6" shape="square"/>
<pad name="40" x="19.05" y="7.62" drill="0.6" shape="square"/>
<pad name="41" x="20.32" y="7.62" drill="0.6" shape="square"/>
<pad name="42" x="21.59" y="7.62" drill="0.6" shape="square"/>
<pad name="43" x="-8.89" y="5.08" drill="0.6" shape="square"/>
<pad name="44" x="-7.62" y="5.08" drill="0.6" shape="square"/>
<pad name="45" x="-6.35" y="5.08" drill="0.6" shape="square"/>
<pad name="46" x="-5.08" y="5.08" drill="0.6" shape="square"/>
<pad name="47" x="2.54" y="3.81" drill="0.6" shape="square"/>
<pad name="48" x="3.81" y="3.81" drill="0.6" shape="square"/>
<pad name="49" x="5.08" y="3.81" drill="0.6" shape="square"/>
<pad name="50" x="7.62" y="3.81" drill="0.6" shape="square"/>
<pad name="51" x="10.16" y="3.81" drill="0.6" shape="square"/>
<pad name="52" x="11.43" y="3.81" drill="0.6" shape="square"/>
<pad name="53" x="12.7" y="3.81" drill="0.6" shape="square"/>
<pad name="54" x="13.97" y="3.81" drill="0.6" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="LOKERCONTROLLER">
<wire x1="-48.26" y1="25.4" x2="-48.26" y2="5.08" width="0.254" layer="94"/>
<wire x1="-48.26" y1="5.08" x2="-48.26" y2="2.54" width="0.254" layer="94"/>
<wire x1="-48.26" y1="2.54" x2="-48.26" y2="0" width="0.254" layer="94"/>
<wire x1="-48.26" y1="0" x2="-48.26" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-48.26" y1="-2.54" x2="-43.18" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-48.26" y1="25.4" x2="195.58" y2="25.4" width="0.254" layer="94"/>
<pin name="TX" x="-5.08" y="-7.62" length="middle" rot="R90"/>
<pin name="RX" x="-2.54" y="-7.62" length="middle" rot="R90"/>
<pin name="GND" x="-7.62" y="-7.62" length="middle" rot="R90"/>
<pin name="VCC" x="0" y="-7.62" length="middle" rot="R90"/>
<wire x1="-48.26" y1="0" x2="-38.1" y2="0" width="0.254" layer="94"/>
<wire x1="-38.1" y1="0" x2="-38.1" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-38.1" y1="-2.54" x2="-43.18" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-43.18" y1="-2.54" x2="-43.18" y2="5.08" width="0.254" layer="94"/>
<wire x1="-43.18" y1="5.08" x2="-48.26" y2="5.08" width="0.254" layer="94"/>
<wire x1="-43.18" y1="5.08" x2="-38.1" y2="5.08" width="0.254" layer="94"/>
<wire x1="-38.1" y1="5.08" x2="-38.1" y2="2.54" width="0.254" layer="94"/>
<wire x1="-38.1" y1="0" x2="-38.1" y2="2.54" width="0.254" layer="94"/>
<wire x1="-38.1" y1="2.54" x2="-48.26" y2="2.54" width="0.254" layer="94"/>
<pin name="DC12" x="-45.72" y="-7.62" length="middle" rot="R90"/>
<pin name="GND12" x="-40.64" y="-7.62" length="middle" rot="R90"/>
<pin name="F.COM.CH1" x="-40.64" y="30.48" length="middle" rot="R270"/>
<pin name="F.INPUT.CH1" x="-38.1" y="30.48" length="middle" rot="R270"/>
<pin name="(+)CH1" x="-35.56" y="30.48" length="middle" rot="R270"/>
<pin name="(-)CH1" x="-33.02" y="30.48" length="middle" rot="R270"/>
<pin name="F.COM.CH2" x="-20.32" y="30.48" length="middle" rot="R270"/>
<pin name="F.INPUT.CH2" x="-17.78" y="30.48" length="middle" rot="R270"/>
<pin name="(+)CH2" x="-15.24" y="30.48" length="middle" rot="R270"/>
<pin name="(-)CH2" x="-12.7" y="30.48" length="middle" rot="R270"/>
<pin name="F.COM.CH3" x="0" y="30.48" length="middle" rot="R270"/>
<pin name="F.INPUT.CH3" x="2.54" y="30.48" length="middle" rot="R270"/>
<pin name="(+)CH3" x="5.08" y="30.48" length="middle" rot="R270"/>
<pin name="(-)CH3" x="7.62" y="30.48" length="middle" rot="R270"/>
<pin name="F.COM.CH4" x="20.32" y="30.48" length="middle" rot="R270"/>
<pin name="F.INPUT.CH4" x="22.86" y="30.48" length="middle" rot="R270"/>
<pin name="(+)CH4" x="25.4" y="30.48" length="middle" rot="R270"/>
<pin name="(-)CH4" x="27.94" y="30.48" length="middle" rot="R270"/>
<wire x1="-38.1" y1="-2.54" x2="195.58" y2="-2.54" width="0.254" layer="94"/>
<pin name="F.COM.CH5" x="40.64" y="30.48" length="middle" rot="R270"/>
<pin name="F.INPUT.CH5" x="43.18" y="30.48" length="middle" rot="R270"/>
<pin name="(+)CH5" x="45.72" y="30.48" length="middle" rot="R270"/>
<pin name="(-)CH5" x="48.26" y="30.48" length="middle" rot="R270"/>
<pin name="F.COM.CH6" x="60.96" y="30.48" length="middle" rot="R270"/>
<pin name="F.INPUT.CH6" x="63.5" y="30.48" length="middle" rot="R270"/>
<pin name="(+)CH6" x="66.04" y="30.48" length="middle" rot="R270"/>
<pin name="(-)CH6" x="68.58" y="30.48" length="middle" rot="R270"/>
<pin name="F.COM.CH7" x="81.28" y="30.48" length="middle" rot="R270"/>
<pin name="F.INPUT.CH7" x="83.82" y="30.48" length="middle" rot="R270"/>
<pin name="(+)CH7" x="86.36" y="30.48" length="middle" rot="R270"/>
<pin name="(-)CH7" x="88.9" y="30.48" length="middle" rot="R270"/>
<pin name="F.COM.CH8" x="101.6" y="30.48" length="middle" rot="R270"/>
<pin name="F.INPUT.CH8" x="104.14" y="30.48" length="middle" rot="R270"/>
<pin name="(+)CH8" x="106.68" y="30.48" length="middle" rot="R270"/>
<pin name="(-)CH8" x="109.22" y="30.48" length="middle" rot="R270"/>
<pin name="F.COM.CH9" x="121.92" y="30.48" length="middle" rot="R270"/>
<pin name="F.INPUT.CH9" x="124.46" y="30.48" length="middle" rot="R270"/>
<pin name="(+)CH9" x="127" y="30.48" length="middle" rot="R270"/>
<pin name="(-)CH9" x="129.54" y="30.48" length="middle" rot="R270"/>
<pin name="F.COM.CH10" x="142.24" y="30.48" length="middle" rot="R270"/>
<pin name="F.INPUT.CH10" x="144.78" y="30.48" length="middle" rot="R270"/>
<pin name="(+)CH10" x="147.32" y="30.48" length="middle" rot="R270"/>
<pin name="(-)CH10" x="149.86" y="30.48" length="middle" rot="R270"/>
<text x="-35.56" y="10.16" size="1.778" layer="94">CH1</text>
<text x="-15.24" y="10.16" size="1.778" layer="94">CH2</text>
<text x="5.08" y="10.16" size="1.778" layer="94">CH3</text>
<text x="5.08" y="10.16" size="1.778" layer="94">CH3</text>
<text x="25.4" y="10.16" size="1.778" layer="94">CH4</text>
<text x="45.72" y="10.16" size="1.778" layer="94">CH5</text>
<text x="66.04" y="10.16" size="1.778" layer="94">CH6</text>
<text x="86.36" y="10.16" size="1.778" layer="94">CH7</text>
<text x="106.68" y="10.16" size="1.778" layer="94">CH8</text>
<text x="127" y="10.16" size="1.778" layer="94">CH9</text>
<text x="147.32" y="10.16" size="1.778" layer="94">CH10</text>
<text x="33.02" y="-7.62" size="1.778" layer="94">LOCKERCONTROLLER</text>
<pin name="F.COM.CH11" x="162.56" y="30.48" length="middle" rot="R270"/>
<pin name="F.INPUT.CH11" x="165.1" y="30.48" length="middle" rot="R270"/>
<pin name="(+)CH11" x="167.64" y="30.48" length="middle" rot="R270"/>
<pin name="(-)CH11" x="170.18" y="30.48" length="middle" rot="R270"/>
<pin name="F.COM.CH12" x="180.34" y="30.48" length="middle" rot="R270"/>
<pin name="F.INPUT.CH12" x="182.88" y="30.48" length="middle" rot="R270"/>
<pin name="(+)CH12" x="185.42" y="30.48" length="middle" rot="R270"/>
<pin name="(-)CH12" x="187.96" y="30.48" length="middle" rot="R270"/>
<wire x1="195.58" y1="25.4" x2="195.58" y2="-2.54" width="0.254" layer="94"/>
<text x="167.64" y="10.16" size="1.778" layer="94">CH11</text>
<text x="185.42" y="10.16" size="1.778" layer="94">CH12</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="LOKERCONTROLLER">
<gates>
<gate name="G$1" symbol="LOKERCONTROLLER" x="-15.24" y="-5.08"/>
</gates>
<devices>
<device name="" package="LOKERCONTROLER">
<connects>
<connect gate="G$1" pin="(+)CH1" pad="9"/>
<connect gate="G$1" pin="(+)CH10" pad="45"/>
<connect gate="G$1" pin="(+)CH11" pad="49"/>
<connect gate="G$1" pin="(+)CH12" pad="54"/>
<connect gate="G$1" pin="(+)CH2" pad="13"/>
<connect gate="G$1" pin="(+)CH3" pad="17"/>
<connect gate="G$1" pin="(+)CH4" pad="21"/>
<connect gate="G$1" pin="(+)CH5" pad="25"/>
<connect gate="G$1" pin="(+)CH6" pad="29"/>
<connect gate="G$1" pin="(+)CH7" pad="33"/>
<connect gate="G$1" pin="(+)CH8" pad="37"/>
<connect gate="G$1" pin="(+)CH9" pad="41"/>
<connect gate="G$1" pin="(-)CH1" pad="10"/>
<connect gate="G$1" pin="(-)CH10" pad="46"/>
<connect gate="G$1" pin="(-)CH11" pad="50"/>
<connect gate="G$1" pin="(-)CH12" pad="53"/>
<connect gate="G$1" pin="(-)CH2" pad="14"/>
<connect gate="G$1" pin="(-)CH3" pad="18"/>
<connect gate="G$1" pin="(-)CH4" pad="22"/>
<connect gate="G$1" pin="(-)CH5" pad="26"/>
<connect gate="G$1" pin="(-)CH6" pad="30"/>
<connect gate="G$1" pin="(-)CH7" pad="34"/>
<connect gate="G$1" pin="(-)CH8" pad="38"/>
<connect gate="G$1" pin="(-)CH9" pad="42"/>
<connect gate="G$1" pin="DC12" pad="1"/>
<connect gate="G$1" pin="F.COM.CH1" pad="7"/>
<connect gate="G$1" pin="F.COM.CH10" pad="43"/>
<connect gate="G$1" pin="F.COM.CH11" pad="47"/>
<connect gate="G$1" pin="F.COM.CH12" pad="51"/>
<connect gate="G$1" pin="F.COM.CH2" pad="11"/>
<connect gate="G$1" pin="F.COM.CH3" pad="15"/>
<connect gate="G$1" pin="F.COM.CH4" pad="19"/>
<connect gate="G$1" pin="F.COM.CH5" pad="23"/>
<connect gate="G$1" pin="F.COM.CH6" pad="27"/>
<connect gate="G$1" pin="F.COM.CH7" pad="31"/>
<connect gate="G$1" pin="F.COM.CH8" pad="35"/>
<connect gate="G$1" pin="F.COM.CH9" pad="39"/>
<connect gate="G$1" pin="F.INPUT.CH1" pad="8"/>
<connect gate="G$1" pin="F.INPUT.CH10" pad="44"/>
<connect gate="G$1" pin="F.INPUT.CH11" pad="48"/>
<connect gate="G$1" pin="F.INPUT.CH12" pad="52"/>
<connect gate="G$1" pin="F.INPUT.CH2" pad="12"/>
<connect gate="G$1" pin="F.INPUT.CH3" pad="16"/>
<connect gate="G$1" pin="F.INPUT.CH4" pad="20"/>
<connect gate="G$1" pin="F.INPUT.CH5" pad="24"/>
<connect gate="G$1" pin="F.INPUT.CH6" pad="28"/>
<connect gate="G$1" pin="F.INPUT.CH7" pad="32"/>
<connect gate="G$1" pin="F.INPUT.CH8" pad="36"/>
<connect gate="G$1" pin="F.INPUT.CH9" pad="40"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="GND12" pad="2"/>
<connect gate="G$1" pin="RX" pad="5"/>
<connect gate="G$1" pin="TX" pad="4"/>
<connect gate="G$1" pin="VCC" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="special" urn="urn:adsk.eagle:library:367">
<description>&lt;b&gt;Special Devices&lt;/b&gt;&lt;p&gt;
7-segment displays, switches, heatsinks, crystals, transformers, etc.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="LDH-5R" urn="urn:adsk.eagle:footprint:26555/1" library_version="3">
<description>&lt;B&gt;LED HOLDER&lt;/B&gt;</description>
<wire x1="-1.27" y1="4.445" x2="0.635" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="5.207" x2="-0.635" y2="3.683" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="3.683" x2="0.635" y2="4.445" width="0.1524" layer="21"/>
<wire x1="0.635" y1="4.445" x2="1.27" y2="4.445" width="0.1524" layer="21"/>
<wire x1="0.635" y1="4.445" x2="-0.635" y2="5.207" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="5.842" x2="0.127" y2="6.604" width="0.1524" layer="21"/>
<wire x1="0" y1="5.461" x2="0.762" y2="6.223" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="6.477" x2="0" y2="6.223" width="0.1524" layer="21"/>
<wire x1="0" y1="6.223" x2="0.127" y2="6.604" width="0.1524" layer="21"/>
<wire x1="0.127" y1="6.604" x2="-0.254" y2="6.477" width="0.1524" layer="21"/>
<wire x1="0.762" y1="6.223" x2="0.381" y2="6.096" width="0.1524" layer="21"/>
<wire x1="0.381" y1="6.096" x2="0.635" y2="5.842" width="0.1524" layer="21"/>
<wire x1="0.635" y1="5.842" x2="0.762" y2="6.223" width="0.1524" layer="21"/>
<wire x1="0.508" y1="6.096" x2="0.635" y2="5.969" width="0.1524" layer="21"/>
<wire x1="-0.127" y1="6.477" x2="0" y2="6.35" width="0.1524" layer="21"/>
<wire x1="0.635" y1="3.683" x2="0.635" y2="5.207" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.524" x2="-2.54" y2="6.604" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="6.604" x2="2.54" y2="6.604" width="0.1524" layer="21" curve="-180"/>
<wire x1="2.54" y1="1.524" x2="2.54" y2="6.604" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.524" x2="-2.54" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.524" x2="3.81" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.524" x2="-3.81" y2="0" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.524" x2="3.81" y2="1.524" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.524" x2="1.905" y2="-6.096" width="0.1524" layer="51"/>
<wire x1="-1.905" y1="1.524" x2="-1.905" y2="-6.096" width="0.1524" layer="51"/>
<wire x1="1.27" y1="4.445" x2="1.27" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="4.445" x2="-1.27" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-6.096" x2="3.302" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.524" x2="-1.27" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.524" x2="1.27" y2="1.524" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.524" x2="2.54" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0" x2="3.302" y2="0" width="0.0508" layer="21"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-6.096" x2="3.302" y2="0" width="0.0508" layer="21"/>
<wire x1="3.302" y1="-6.096" x2="3.81" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0" x2="-4.318" y2="0" width="0.0508" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="-6.096" x2="-4.318" y2="0" width="0.0508" layer="21"/>
<wire x1="-4.318" y1="-6.096" x2="-3.81" y2="-6.096" width="0.0508" layer="21"/>
<circle x="2.54" y="0" radius="0.635" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="0.635" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="-5.08" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="-5.08" drill="0.8128" shape="long" rot="R90"/>
<text x="-4.318" y="-7.747" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-9.271" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<hole x="2.54" y="0" drill="1.2192"/>
<hole x="-2.54" y="0" drill="1.2192"/>
</package>
</packages>
<packages3d>
<package3d name="LDH-5R" urn="urn:adsk.eagle:package:26651/1" type="box" library_version="3">
<description>LED HOLDER</description>
<packageinstances>
<packageinstance name="LDH-5R"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="LAMP" urn="urn:adsk.eagle:symbol:26554/1" library_version="3">
<wire x1="4.064" y1="2.286" x2="4.064" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="4.064" y1="-2.54" x2="2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-1.524" y1="-2.54" x2="-1.524" y2="2.286" width="0.4064" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.524" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="0" y1="1.27" x2="1.2959" y2="2.5651" width="0.254" layer="94" curve="-92.32742" cap="flat"/>
<wire x1="1.2954" y1="2.54" x2="2.5654" y2="1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-1.524" y1="2.286" x2="1.2957" y2="5.1055" width="0.4064" layer="94" curve="-91.045739" cap="flat"/>
<wire x1="1.2954" y1="5.08" x2="4.0894" y2="2.286" width="0.4064" layer="94" curve="-90" cap="flat"/>
<text x="-2.54" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="6.985" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="1" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="2" x="2.54" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LAMP" urn="urn:adsk.eagle:component:26675/2" prefix="LMP" uservalue="yes" library_version="3">
<description>&lt;B&gt;LED HOLDER&lt;/B&gt;</description>
<gates>
<gate name="G$1" symbol="LAMP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LDH-5R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26651/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2" urn="urn:adsk.eagle:library:372">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26990/1" library_version="2">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:27037/1" prefix="SUPPLY" library_version="2">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="loker">
<packages>
<package name="LOKER">
<pad name="1" x="-6.35" y="5.08" drill="0.6" shape="square"/>
<pad name="2" x="-6.35" y="3.81" drill="0.6" shape="square"/>
<pad name="3" x="-6.35" y="2.54" drill="0.6" shape="square"/>
<pad name="4" x="-6.35" y="1.27" drill="0.6" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="LOKER">
<wire x1="-22.86" y1="17.78" x2="-22.86" y2="5.08" width="0.1524" layer="94"/>
<pin name="FCOM" x="-27.94" y="15.24" length="middle"/>
<pin name="FIN" x="-27.94" y="12.7" length="middle"/>
<pin name="VCC" x="-27.94" y="10.16" length="middle"/>
<pin name="GND" x="-27.94" y="7.62" length="middle"/>
<wire x1="-15.24" y1="17.78" x2="-22.86" y2="17.78" width="0.1524" layer="94"/>
<wire x1="-22.86" y1="5.08" x2="-15.24" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-15.24" y1="5.08" x2="-15.24" y2="17.78" width="0.1524" layer="94"/>
<text x="-10.16" y="7.62" size="1.778" layer="94" rot="R90">LOCKER</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="LOKER">
<gates>
<gate name="G$1" symbol="LOKER" x="-2.54" y="2.54"/>
</gates>
<devices>
<device name="" package="LOKER">
<connects>
<connect gate="G$1" pin="FCOM" pad="1"/>
<connect gate="G$1" pin="FIN" pad="2"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="VCC" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply">
<packages>
<package name="SUPPLY">
<pad name="1" x="-15.24" y="3.81" drill="0.6" shape="square"/>
<pad name="2" x="-11.43" y="3.81" drill="0.6" shape="square"/>
<pad name="3" x="-7.62" y="3.81" drill="0.6" shape="square"/>
<pad name="4" x="-2.54" y="3.81" drill="0.6" shape="square"/>
<pad name="5" x="1.27" y="3.81" drill="0.6" shape="square"/>
<pad name="6" x="3.81" y="3.81" drill="0.6" shape="square"/>
<pad name="7" x="8.89" y="3.81" drill="0.6" shape="square"/>
<pad name="8" x="-3.81" y="6.35" drill="0.6" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="SUPPLY">
<wire x1="-40.64" y1="12.7" x2="-40.64" y2="2.54" width="0.254" layer="94"/>
<wire x1="-40.64" y1="2.54" x2="-20.32" y2="2.54" width="0.254" layer="94"/>
<wire x1="-20.32" y1="2.54" x2="-20.32" y2="12.7" width="0.254" layer="94"/>
<pin name="L(+)" x="-38.1" y="-2.54" length="middle" rot="R90"/>
<pin name="N(-)" x="-35.56" y="-2.54" length="middle" rot="R90"/>
<pin name="FG" x="-33.02" y="-2.54" length="middle" rot="R90"/>
<pin name="GND" x="-30.48" y="-2.54" length="middle" rot="R90"/>
<pin name="+V2" x="-27.94" y="-2.54" length="middle" rot="R90"/>
<pin name="GND." x="-25.4" y="-2.54" length="middle" rot="R90"/>
<pin name="+V1" x="-22.86" y="-2.54" length="middle" rot="R90"/>
<text x="-43.18" y="2.54" size="1.778" layer="94" rot="R90">SUPPLY</text>
<wire x1="-40.64" y1="12.7" x2="-40.64" y2="17.78" width="0.254" layer="94"/>
<wire x1="-40.64" y1="17.78" x2="-20.32" y2="17.78" width="0.254" layer="94"/>
<wire x1="-20.32" y1="17.78" x2="-20.32" y2="12.7" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SUPPLY">
<gates>
<gate name="G$1" symbol="SUPPLY" x="7.62" y="7.62"/>
</gates>
<devices>
<device name="" package="SUPPLY">
<connects>
<connect gate="G$1" pin="+V1" pad="7"/>
<connect gate="G$1" pin="+V2" pad="5"/>
<connect gate="G$1" pin="FG" pad="3"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="GND." pad="6"/>
<connect gate="G$1" pin="L(+)" pad="1"/>
<connect gate="G$1" pin="N(-)" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="FAN">
<packages>
<package name="FAN">
<pad name="1" x="-22.86" y="0" drill="0.6" shape="square"/>
<pad name="2" x="-15.24" y="-1.27" drill="0.6" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="FAN">
<wire x1="-40.64" y1="10.16" x2="-40.64" y2="2.54" width="0.254" layer="94"/>
<wire x1="-40.64" y1="2.54" x2="-30.48" y2="2.54" width="0.254" layer="94"/>
<wire x1="-30.48" y1="2.54" x2="-30.48" y2="10.16" width="0.254" layer="94"/>
<wire x1="-30.48" y1="10.16" x2="-40.64" y2="10.16" width="0.254" layer="94"/>
<pin name="VCC" x="-38.1" y="-2.54" length="middle" rot="R90"/>
<pin name="GND" x="-33.02" y="-2.54" length="middle" rot="R90"/>
<text x="-43.18" y="5.08" size="1.778" layer="94" rot="R90">FAN</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="FAN">
<gates>
<gate name="G$1" symbol="FAN" x="22.86" y="7.62"/>
</gates>
<devices>
<device name="" package="FAN">
<connects>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BX625CI-MS">
<packages>
<package name="BX625CI-MS">
<pad name="1" x="-24.13" y="2.54" drill="0.6" shape="square"/>
<pad name="2" x="-17.78" y="2.54" drill="0.6" shape="square"/>
<pad name="3" x="-10.16" y="2.54" drill="0.6" shape="square"/>
<pad name="4" x="-2.54" y="2.54" drill="0.6" shape="square"/>
<pad name="5" x="-24.13" y="1.27" drill="0.6" shape="square"/>
<pad name="6" x="-19.05" y="1.27" drill="0.6" shape="square"/>
<pad name="7" x="-16.51" y="1.27" drill="0.6" shape="square"/>
<pad name="8" x="-15.24" y="1.27" drill="0.6" shape="square"/>
<pad name="9" x="-12.7" y="1.27" drill="0.6" shape="square"/>
<pad name="10" x="-11.43" y="0" drill="0.6" shape="square"/>
<pad name="11" x="-8.89" y="0" drill="0.6" shape="square"/>
<pad name="12" x="-6.35" y="0" drill="0.6" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="BX625CI-MS">
<text x="-20.32" y="27.94" size="1.778" layer="94" rot="R180">BX625CI-MS</text>
<wire x1="-38.1" y1="25.4" x2="-17.78" y2="25.4" width="0.254" layer="94"/>
<wire x1="-17.78" y1="25.4" x2="-17.78" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-38.1" y1="25.4" x2="-38.1" y2="-5.08" width="0.254" layer="94"/>
<pin name="L(+)OUT1" x="-43.18" y="22.86" length="middle"/>
<pin name="N(-)OUT1" x="-43.18" y="20.32" length="middle"/>
<pin name="FG.OUT1" x="-43.18" y="17.78" length="middle"/>
<pin name="L(+)OUT2" x="-43.18" y="12.7" length="middle"/>
<pin name="N(-)OUT2" x="-43.18" y="10.16" length="middle"/>
<pin name="FG.OUT2" x="-43.18" y="7.62" length="middle"/>
<pin name="L(+)OUT3" x="-43.18" y="2.54" length="middle"/>
<pin name="N(-)OUT3" x="-43.18" y="0" length="middle"/>
<pin name="FG.OUT3" x="-43.18" y="-2.54" length="middle"/>
<pin name="L(+)" x="-12.7" y="12.7" length="middle" rot="R180"/>
<pin name="N(-)" x="-12.7" y="10.16" length="middle" rot="R180"/>
<pin name="FG" x="-12.7" y="7.62" length="middle" rot="R180"/>
<wire x1="-38.1" y1="-5.08" x2="-17.78" y2="-5.08" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BX625CI-MS">
<gates>
<gate name="G$1" symbol="BX625CI-MS" x="2.54" y="-2.54"/>
</gates>
<devices>
<device name="" package="BX625CI-MS">
<connects>
<connect gate="G$1" pin="FG" pad="3"/>
<connect gate="G$1" pin="FG.OUT1" pad="6"/>
<connect gate="G$1" pin="FG.OUT2" pad="9"/>
<connect gate="G$1" pin="FG.OUT3" pad="12"/>
<connect gate="G$1" pin="L(+)" pad="1"/>
<connect gate="G$1" pin="L(+)OUT1" pad="4"/>
<connect gate="G$1" pin="L(+)OUT2" pad="7"/>
<connect gate="G$1" pin="L(+)OUT3" pad="10"/>
<connect gate="G$1" pin="N(-)" pad="2"/>
<connect gate="G$1" pin="N(-)OUT1" pad="5"/>
<connect gate="G$1" pin="N(-)OUT2" pad="8"/>
<connect gate="G$1" pin="N(-)OUT3" pad="11"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="raspberrry b+">
<packages>
<package name="RASPBERRYB+">
<pad name="1" x="-26.67" y="10.16" drill="0.6" shape="square"/>
<pad name="2" x="-25.4" y="10.16" drill="0.6" shape="square"/>
<pad name="3" x="-24.13" y="10.16" drill="0.6" shape="square"/>
<pad name="4" x="-22.86" y="10.16" drill="0.6" shape="square"/>
<pad name="5" x="-21.59" y="10.16" drill="0.6" shape="square"/>
<pad name="6" x="-20.32" y="10.16" drill="0.6" shape="square"/>
<pad name="7" x="-19.05" y="10.16" drill="0.6" shape="square"/>
<pad name="8" x="-17.78" y="10.16" drill="0.6" shape="square"/>
<pad name="9" x="-16.51" y="10.16" drill="0.6" shape="square"/>
<pad name="10" x="-15.24" y="10.16" drill="0.6" shape="square"/>
<pad name="11" x="-26.67" y="8.89" drill="0.6" shape="square"/>
<pad name="12" x="-25.4" y="8.89" drill="0.6" shape="square"/>
<pad name="13" x="-24.13" y="8.89" drill="0.6" shape="square"/>
<pad name="14" x="-22.86" y="8.89" drill="0.6" shape="square"/>
<pad name="15" x="-21.59" y="8.89" drill="0.6" shape="square"/>
<pad name="16" x="-20.32" y="8.89" drill="0.6" shape="square"/>
<pad name="17" x="-19.05" y="8.89" drill="0.6" shape="square"/>
<pad name="18" x="-17.78" y="8.89" drill="0.6" shape="square"/>
<pad name="19" x="-16.51" y="8.89" drill="0.6" shape="square"/>
<pad name="20" x="-15.24" y="8.89" drill="0.6" shape="square"/>
<pad name="21" x="-26.67" y="7.62" drill="0.6" shape="square"/>
<pad name="22" x="-25.4" y="7.62" drill="0.6" shape="square"/>
<pad name="23" x="-24.13" y="7.62" drill="0.6" shape="square"/>
<pad name="24" x="-22.86" y="7.62" drill="0.6" shape="square"/>
<pad name="25" x="-21.59" y="7.62" drill="0.6" shape="square"/>
<pad name="26" x="-20.32" y="7.62" drill="0.6" shape="square"/>
<pad name="27" x="-19.05" y="7.62" drill="0.6" shape="square"/>
<pad name="28" x="-17.78" y="7.62" drill="0.6" shape="square"/>
<pad name="29" x="-16.51" y="7.62" drill="0.6" shape="square"/>
<pad name="30" x="-15.24" y="7.62" drill="0.6" shape="square"/>
<pad name="31" x="-26.67" y="6.35" drill="0.6" shape="square"/>
<pad name="32" x="-25.4" y="6.35" drill="0.6" shape="square"/>
<pad name="33" x="-24.13" y="6.35" drill="0.6" shape="square"/>
<pad name="34" x="-22.86" y="6.35" drill="0.6" shape="square"/>
<pad name="35" x="-21.59" y="6.35" drill="0.6" shape="square"/>
<pad name="36" x="-20.32" y="6.35" drill="0.6" shape="square"/>
<pad name="37" x="-19.05" y="6.35" drill="0.6" shape="square"/>
<pad name="38" x="-17.78" y="6.35" drill="0.6" shape="square"/>
<pad name="40" x="-15.24" y="6.35" drill="0.6" shape="square"/>
<pad name="39" x="-16.51" y="6.35" drill="0.6" shape="square"/>
<pad name="41" x="-26.67" y="5.08" drill="0.6" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="RASPBERRYPIB+">
<pin name="3.3V" x="-35.56" y="27.94" length="middle"/>
<pin name="5.0V" x="5.08" y="27.94" length="middle" rot="R180"/>
<pin name="SDA" x="-35.56" y="25.4" length="middle"/>
<pin name="5.0V." x="5.08" y="25.4" length="middle" rot="R180"/>
<pin name="SCL" x="-35.56" y="22.86" length="middle"/>
<pin name="GND1" x="5.08" y="22.86" length="middle" rot="R180"/>
<pin name="GPIO4" x="-35.56" y="20.32" length="middle"/>
<pin name="GND2" x="-35.56" y="17.78" length="middle"/>
<pin name="GPIO17" x="-35.56" y="15.24" length="middle"/>
<pin name="GPIO27" x="-35.56" y="12.7" length="middle"/>
<pin name="GPIO22" x="-35.56" y="10.16" length="middle"/>
<pin name="3.3V." x="-35.56" y="7.62" length="middle"/>
<pin name="SPI_MOSI" x="-35.56" y="5.08" length="middle"/>
<pin name="SPI_SCLK" x="-35.56" y="0" length="middle"/>
<pin name="SPI_MISO" x="-35.56" y="2.54" length="middle"/>
<pin name="GND5" x="-35.56" y="-2.54" length="middle"/>
<pin name="EEDATA" x="-35.56" y="-5.08" length="middle"/>
<pin name="GPIO5" x="-35.56" y="-7.62" length="middle"/>
<pin name="GPIO6" x="-35.56" y="-10.16" length="middle"/>
<pin name="GPIO13" x="-35.56" y="-12.7" length="middle"/>
<pin name="GPIO19" x="-35.56" y="-15.24" length="middle"/>
<pin name="GPIO26" x="-35.56" y="-17.78" length="middle"/>
<pin name="GND8" x="-35.56" y="-20.32" length="middle"/>
<pin name="TXD" x="5.08" y="20.32" length="middle" rot="R180"/>
<pin name="RXD" x="5.08" y="17.78" length="middle" rot="R180"/>
<pin name="GPIO18" x="5.08" y="15.24" length="middle" rot="R180"/>
<pin name="GND3" x="5.08" y="12.7" length="middle" rot="R180"/>
<pin name="GPIO23" x="5.08" y="10.16" length="middle" rot="R180"/>
<pin name="GPIO24" x="5.08" y="7.62" length="middle" rot="R180"/>
<pin name="GND4" x="5.08" y="5.08" length="middle" rot="R180"/>
<pin name="GPIO25" x="5.08" y="2.54" length="middle" rot="R180"/>
<pin name="#SPI_CE0" x="5.08" y="0" length="middle" rot="R180"/>
<pin name="#SPI_CE1" x="5.08" y="-2.54" length="middle" rot="R180"/>
<pin name="EECLK" x="5.08" y="-5.08" length="middle" rot="R180"/>
<pin name="GND6" x="5.08" y="-7.62" length="middle" rot="R180"/>
<pin name="GPIO12" x="5.08" y="-10.16" length="middle" rot="R180"/>
<pin name="GND7" x="5.08" y="-12.7" length="middle" rot="R180"/>
<pin name="GPIO16" x="5.08" y="-15.24" length="middle" rot="R180"/>
<pin name="GPIO20" x="5.08" y="-17.78" length="middle" rot="R180"/>
<pin name="GPIO21" x="5.08" y="-20.32" length="middle" rot="R180"/>
<wire x1="-30.48" y1="30.48" x2="-30.48" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-30.48" y1="-22.86" x2="0" y2="-22.86" width="0.254" layer="94"/>
<wire x1="0" y1="-22.86" x2="0" y2="30.48" width="0.254" layer="94"/>
<wire x1="0" y1="30.48" x2="-30.48" y2="30.48" width="0.254" layer="94"/>
<text x="-27.94" y="33.02" size="1.778" layer="94">Raspberry Pi Model B+</text>
<pin name="USB" x="-15.24" y="-27.94" length="middle" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RASPBERRYPI">
<gates>
<gate name="G$1" symbol="RASPBERRYPIB+" x="-10.16" y="-2.54"/>
</gates>
<devices>
<device name="" package="RASPBERRYB+">
<connects>
<connect gate="G$1" pin="#SPI_CE0" pad="24"/>
<connect gate="G$1" pin="#SPI_CE1" pad="26"/>
<connect gate="G$1" pin="3.3V" pad="1"/>
<connect gate="G$1" pin="3.3V." pad="17"/>
<connect gate="G$1" pin="5.0V" pad="2"/>
<connect gate="G$1" pin="5.0V." pad="4"/>
<connect gate="G$1" pin="EECLK" pad="28"/>
<connect gate="G$1" pin="EEDATA" pad="27"/>
<connect gate="G$1" pin="GND1" pad="6"/>
<connect gate="G$1" pin="GND2" pad="9"/>
<connect gate="G$1" pin="GND3" pad="14"/>
<connect gate="G$1" pin="GND4" pad="20"/>
<connect gate="G$1" pin="GND5" pad="25"/>
<connect gate="G$1" pin="GND6" pad="30"/>
<connect gate="G$1" pin="GND7" pad="34"/>
<connect gate="G$1" pin="GND8" pad="39"/>
<connect gate="G$1" pin="GPIO12" pad="32"/>
<connect gate="G$1" pin="GPIO13" pad="33"/>
<connect gate="G$1" pin="GPIO16" pad="36"/>
<connect gate="G$1" pin="GPIO17" pad="11"/>
<connect gate="G$1" pin="GPIO18" pad="12"/>
<connect gate="G$1" pin="GPIO19" pad="35"/>
<connect gate="G$1" pin="GPIO20" pad="38"/>
<connect gate="G$1" pin="GPIO21" pad="40"/>
<connect gate="G$1" pin="GPIO22" pad="15"/>
<connect gate="G$1" pin="GPIO23" pad="16"/>
<connect gate="G$1" pin="GPIO24" pad="18"/>
<connect gate="G$1" pin="GPIO25" pad="22"/>
<connect gate="G$1" pin="GPIO26" pad="37"/>
<connect gate="G$1" pin="GPIO27" pad="13"/>
<connect gate="G$1" pin="GPIO4" pad="7"/>
<connect gate="G$1" pin="GPIO5" pad="29"/>
<connect gate="G$1" pin="GPIO6" pad="31"/>
<connect gate="G$1" pin="RXD" pad="10"/>
<connect gate="G$1" pin="SCL" pad="5"/>
<connect gate="G$1" pin="SDA" pad="3"/>
<connect gate="G$1" pin="SPI_MISO" pad="21"/>
<connect gate="G$1" pin="SPI_MOSI" pad="19"/>
<connect gate="G$1" pin="SPI_SCLK" pad="23"/>
<connect gate="G$1" pin="TXD" pad="8"/>
<connect gate="G$1" pin="USB" pad="41"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ngspice-simulation" urn="urn:adsk.eagle:library:527439">
<description>SPICE compatible library parts</description>
<packages>
</packages>
<symbols>
<symbol name="0" urn="urn:adsk.eagle:symbol:527455/1" library_version="18">
<description>Simulation ground symbol (spice node 0)</description>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<pin name="0" x="0" y="0" visible="off" length="point" direction="sup"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:527478/1" prefix="X_" library_version="18">
<description>Simulation ground symbol (spice node 0)</description>
<gates>
<gate name="G$1" symbol="0" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="SPICEGROUND" value=""/>
<attribute name="_EXTERNAL_" value=""/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="TXS0108E">
<packages>
<package name="TXS0108E">
<pad name="1" x="-22.86" y="8.89" drill="0.6" shape="square"/>
<pad name="2" x="-22.86" y="7.62" drill="0.6" shape="square"/>
<pad name="3" x="-22.86" y="6.35" drill="0.6" shape="square"/>
<pad name="4" x="-22.86" y="5.08" drill="0.6" shape="square"/>
<pad name="5" x="-22.86" y="3.81" drill="0.6" shape="square"/>
<pad name="6" x="-22.86" y="2.54" drill="0.6" shape="square"/>
<pad name="7" x="-22.86" y="1.27" drill="0.6" shape="square"/>
<pad name="8" x="-22.86" y="0" drill="0.6" shape="square"/>
<pad name="9" x="-22.86" y="-2.54" drill="0.6" shape="square"/>
<pad name="10" x="-22.86" y="-3.81" drill="0.6" shape="square"/>
<pad name="11" x="-20.32" y="8.89" drill="0.6" shape="square"/>
<pad name="12" x="-20.32" y="7.62" drill="0.6" shape="square"/>
<pad name="13" x="-20.32" y="6.35" drill="0.6" shape="square"/>
<pad name="14" x="-20.32" y="5.08" drill="0.6" shape="square"/>
<pad name="15" x="-20.32" y="3.81" drill="0.6" shape="square"/>
<pad name="16" x="-20.32" y="2.54" drill="0.6" shape="square"/>
<pad name="17" x="-20.32" y="1.27" drill="0.6" shape="square"/>
<pad name="18" x="-20.32" y="0" drill="0.6" shape="square"/>
<pad name="19" x="-20.32" y="-2.54" drill="0.6" shape="square"/>
<pad name="20" x="-20.32" y="-3.81" drill="0.6" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="TXS0108E">
<pin name="B1" x="-33.02" y="22.86" length="middle"/>
<pin name="B2" x="-33.02" y="20.32" length="middle"/>
<pin name="B3" x="-33.02" y="17.78" length="middle"/>
<pin name="B4" x="-33.02" y="15.24" length="middle"/>
<pin name="B5" x="-33.02" y="12.7" length="middle"/>
<pin name="B6" x="-33.02" y="10.16" length="middle"/>
<pin name="B7" x="-33.02" y="7.62" length="middle"/>
<pin name="B8" x="-33.02" y="5.08" length="middle"/>
<pin name="VCCB" x="-33.02" y="0" length="middle"/>
<pin name="VCCA" x="-33.02" y="-2.54" length="middle"/>
<pin name="A1" x="-2.54" y="22.86" length="middle" rot="R180"/>
<pin name="A2" x="-2.54" y="20.32" length="middle" rot="R180"/>
<pin name="A3" x="-2.54" y="17.78" length="middle" rot="R180"/>
<pin name="A4" x="-2.54" y="15.24" length="middle" rot="R180"/>
<pin name="A5" x="-2.54" y="12.7" length="middle" rot="R180"/>
<pin name="A6" x="-2.54" y="10.16" length="middle" rot="R180"/>
<pin name="A7" x="-2.54" y="7.62" length="middle" rot="R180"/>
<pin name="A8" x="-2.54" y="5.08" length="middle" rot="R180"/>
<pin name="OE" x="-2.54" y="0" length="middle" rot="R180"/>
<pin name="GND" x="-2.54" y="-2.54" length="middle" rot="R180"/>
<wire x1="-27.94" y1="25.4" x2="-27.94" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-27.94" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="25.4" width="0.254" layer="94"/>
<wire x1="-7.62" y1="25.4" x2="-27.94" y2="25.4" width="0.254" layer="94"/>
<text x="-25.4" y="27.94" size="1.778" layer="94"> TXS0108E</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="TXS0108E">
<gates>
<gate name="G$1" symbol="TXS0108E" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="TXS0108E">
<connects>
<connect gate="G$1" pin="A1" pad="1"/>
<connect gate="G$1" pin="A2" pad="2"/>
<connect gate="G$1" pin="A3" pad="3"/>
<connect gate="G$1" pin="A4" pad="4"/>
<connect gate="G$1" pin="A5" pad="5"/>
<connect gate="G$1" pin="A6" pad="6"/>
<connect gate="G$1" pin="A7" pad="7"/>
<connect gate="G$1" pin="A8" pad="8"/>
<connect gate="G$1" pin="B1" pad="11"/>
<connect gate="G$1" pin="B2" pad="12"/>
<connect gate="G$1" pin="B3" pad="13"/>
<connect gate="G$1" pin="B4" pad="14"/>
<connect gate="G$1" pin="B5" pad="15"/>
<connect gate="G$1" pin="B6" pad="16"/>
<connect gate="G$1" pin="B7" pad="17"/>
<connect gate="G$1" pin="B8" pad="18"/>
<connect gate="G$1" pin="GND" pad="10"/>
<connect gate="G$1" pin="OE" pad="9"/>
<connect gate="G$1" pin="VCCA" pad="20"/>
<connect gate="G$1" pin="VCCB" pad="19"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="RS485USB">
<packages>
<package name="RS485USB">
<pad name="1" x="-16.51" y="-1.27" drill="0.6" shape="square"/>
<pad name="2" x="-16.51" y="-2.54" drill="0.6" shape="square"/>
<pad name="3" x="-16.51" y="-3.81" drill="0.6" shape="square"/>
<pad name="4" x="-16.51" y="-5.08" drill="0.6" shape="square"/>
<pad name="5" x="-16.51" y="-6.35" drill="0.6" shape="square"/>
<pad name="6" x="-11.43" y="-3.81" drill="0.6" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="RS485USB">
<pin name="5V" x="-25.4" y="17.78" length="middle"/>
<pin name="3.3V" x="-25.4" y="15.24" length="middle"/>
<pin name="TXD" x="-25.4" y="12.7" length="middle"/>
<pin name="RXD" x="-25.4" y="10.16" length="middle"/>
<pin name="GND" x="-25.4" y="7.62" length="middle"/>
<pin name="USB" x="0" y="12.7" length="middle" rot="R180"/>
<wire x1="-20.32" y1="20.32" x2="-20.32" y2="5.08" width="0.254" layer="94"/>
<wire x1="-20.32" y1="5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="20.32" width="0.254" layer="94"/>
<wire x1="-5.08" y1="20.32" x2="-20.32" y2="20.32" width="0.254" layer="94"/>
<text x="-20.32" y="22.86" size="1.778" layer="94">RS485-USB</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RS485USB">
<gates>
<gate name="G$1" symbol="RS485USB" x="12.7" y="7.62"/>
</gates>
<devices>
<device name="" package="RS485USB">
<connects>
<connect gate="G$1" pin="3.3V" pad="2"/>
<connect gate="G$1" pin="5V" pad="1"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="RXD" pad="4"/>
<connect gate="G$1" pin="TXD" pad="3"/>
<connect gate="G$1" pin="USB" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$1" library="relay16channel" deviceset="RELAY16CHANNEL" device=""/>
<part name="U$2" library="lokercontroller" deviceset="LOKERCONTROLLER" device=""/>
<part name="LMP1" library="special" library_urn="urn:adsk.eagle:library:367" deviceset="LAMP" device="" package3d_urn="urn:adsk.eagle:package:26651/1"/>
<part name="LMP2" library="special" library_urn="urn:adsk.eagle:library:367" deviceset="LAMP" device="" package3d_urn="urn:adsk.eagle:package:26651/1"/>
<part name="LMP3" library="special" library_urn="urn:adsk.eagle:library:367" deviceset="LAMP" device="" package3d_urn="urn:adsk.eagle:package:26651/1"/>
<part name="LMP5" library="special" library_urn="urn:adsk.eagle:library:367" deviceset="LAMP" device="" package3d_urn="urn:adsk.eagle:package:26651/1"/>
<part name="LMP7" library="special" library_urn="urn:adsk.eagle:library:367" deviceset="LAMP" device="" package3d_urn="urn:adsk.eagle:package:26651/1"/>
<part name="LMP8" library="special" library_urn="urn:adsk.eagle:library:367" deviceset="LAMP" device="" package3d_urn="urn:adsk.eagle:package:26651/1"/>
<part name="LMP6" library="special" library_urn="urn:adsk.eagle:library:367" deviceset="LAMP" device="" package3d_urn="urn:adsk.eagle:package:26651/1"/>
<part name="LMP4" library="special" library_urn="urn:adsk.eagle:library:367" deviceset="LAMP" device="" package3d_urn="urn:adsk.eagle:package:26651/1"/>
<part name="LMP9" library="special" library_urn="urn:adsk.eagle:library:367" deviceset="LAMP" device="" package3d_urn="urn:adsk.eagle:package:26651/1"/>
<part name="LMP10" library="special" library_urn="urn:adsk.eagle:library:367" deviceset="LAMP" device="" package3d_urn="urn:adsk.eagle:package:26651/1"/>
<part name="SUPPLY3" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY2" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="U$3" library="loker" deviceset="LOKER" device=""/>
<part name="U$4" library="loker" deviceset="LOKER" device=""/>
<part name="U$5" library="loker" deviceset="LOKER" device=""/>
<part name="U$6" library="loker" deviceset="LOKER" device=""/>
<part name="U$7" library="loker" deviceset="LOKER" device=""/>
<part name="U$8" library="loker" deviceset="LOKER" device=""/>
<part name="U$9" library="loker" deviceset="LOKER" device=""/>
<part name="U$10" library="loker" deviceset="LOKER" device=""/>
<part name="U$11" library="loker" deviceset="LOKER" device=""/>
<part name="U$12" library="loker" deviceset="LOKER" device=""/>
<part name="U$13" library="supply" deviceset="SUPPLY" device=""/>
<part name="U$14" library="FAN" deviceset="FAN" device=""/>
<part name="U$15" library="FAN" deviceset="FAN" device=""/>
<part name="U$16" library="BX625CI-MS" deviceset="BX625CI-MS" device=""/>
<part name="U$17" library="raspberrry b+" deviceset="RASPBERRYPI" device=""/>
<part name="X_1" library="ngspice-simulation" library_urn="urn:adsk.eagle:library:527439" deviceset="GND" device=""/>
<part name="U$18" library="TXS0108E" deviceset="TXS0108E" device=""/>
<part name="U$19" library="TXS0108E" deviceset="TXS0108E" device=""/>
<part name="U$20" library="RS485USB" deviceset="RS485USB" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U$1" gate="G$1" x="-25.4" y="50.8" smashed="yes" rot="R90"/>
<instance part="U$2" gate="G$1" x="40.64" y="-30.48" smashed="yes" rot="R180"/>
<instance part="LMP1" gate="G$1" x="-185.42" y="2.54" smashed="yes">
<attribute name="NAME" x="-187.96" y="0" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-178.435" y="0" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="LMP2" gate="G$1" x="-165.1" y="2.54" smashed="yes">
<attribute name="NAME" x="-167.64" y="0" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-158.115" y="0" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="LMP3" gate="G$1" x="-144.78" y="2.54" smashed="yes">
<attribute name="NAME" x="-147.32" y="0" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-137.795" y="0" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="LMP5" gate="G$1" x="-104.14" y="2.54" smashed="yes">
<attribute name="NAME" x="-106.68" y="0" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-97.155" y="0" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="LMP7" gate="G$1" x="-63.5" y="2.54" smashed="yes">
<attribute name="NAME" x="-66.04" y="0" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-56.515" y="0" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="LMP8" gate="G$1" x="-43.18" y="2.54" smashed="yes">
<attribute name="NAME" x="-45.72" y="0" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-36.195" y="0" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="LMP6" gate="G$1" x="-83.82" y="2.54" smashed="yes">
<attribute name="NAME" x="-86.36" y="0" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-76.835" y="0" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="LMP4" gate="G$1" x="-124.46" y="2.54" smashed="yes">
<attribute name="NAME" x="-127" y="0" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-117.475" y="0" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="LMP9" gate="G$1" x="-43.18" y="101.6" smashed="yes">
<attribute name="NAME" x="-45.72" y="99.06" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-36.195" y="99.06" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="LMP10" gate="G$1" x="-63.5" y="101.6" smashed="yes">
<attribute name="NAME" x="-66.04" y="99.06" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-56.515" y="99.06" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY3" gate="GND" x="-30.48" y="91.44" smashed="yes">
<attribute name="VALUE" x="-32.385" y="88.265" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY2" gate="GND" x="-116.84" y="-15.24" smashed="yes">
<attribute name="VALUE" x="-118.745" y="-18.415" size="1.778" layer="96"/>
</instance>
<instance part="U$3" gate="G$1" x="-116.84" y="-93.98" smashed="yes" rot="R270"/>
<instance part="U$4" gate="G$1" x="-96.52" y="-93.98" smashed="yes" rot="R270"/>
<instance part="U$5" gate="G$1" x="-76.2" y="-93.98" smashed="yes" rot="R270"/>
<instance part="U$6" gate="G$1" x="-55.88" y="-93.98" smashed="yes" rot="R270"/>
<instance part="U$7" gate="G$1" x="-35.56" y="-93.98" smashed="yes" rot="R270"/>
<instance part="U$8" gate="G$1" x="-15.24" y="-93.98" smashed="yes" rot="R270"/>
<instance part="U$9" gate="G$1" x="5.08" y="-93.98" smashed="yes" rot="R270"/>
<instance part="U$10" gate="G$1" x="25.4" y="-93.98" smashed="yes" rot="R270"/>
<instance part="U$11" gate="G$1" x="45.72" y="-93.98" smashed="yes" rot="R270"/>
<instance part="U$12" gate="G$1" x="66.04" y="-93.98" smashed="yes" rot="R270"/>
<instance part="U$13" gate="G$1" x="38.1" y="-25.4" smashed="yes" rot="R270"/>
<instance part="U$14" gate="G$1" x="-198.12" y="-76.2" smashed="yes" rot="R270"/>
<instance part="U$15" gate="G$1" x="-172.72" y="-76.2" smashed="yes" rot="R270"/>
<instance part="U$16" gate="G$1" x="27.94" y="-5.08" smashed="yes"/>
<instance part="U$17" gate="G$1" x="40.64" y="53.34" smashed="yes"/>
<instance part="X_1" gate="G$1" x="2.54" y="27.94" smashed="yes"/>
<instance part="U$18" gate="G$1" x="101.6" y="58.42" smashed="yes"/>
<instance part="U$19" gate="G$1" x="101.6" y="22.86" smashed="yes"/>
<instance part="U$20" gate="G$1" x="93.98" y="-10.16" smashed="yes"/>
</instances>
<busses>
</busses>
<nets>
<net name="5V" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="VCC"/>
<wire x1="40.64" y1="-22.86" x2="40.64" y2="-17.78" width="0.1524" layer="91"/>
<label x="40.64" y="-20.32" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="U$13" gate="G$1" pin="+V2"/>
<wire x1="35.56" y1="2.54" x2="30.48" y2="2.54" width="0.1524" layer="91"/>
<label x="30.48" y="2.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$17" gate="G$1" pin="5.0V."/>
<wire x1="45.72" y1="78.74" x2="48.26" y2="78.74" width="0.1524" layer="91"/>
<wire x1="48.26" y1="78.74" x2="48.26" y2="81.28" width="0.1524" layer="91"/>
<pinref part="U$17" gate="G$1" pin="5.0V"/>
<wire x1="48.26" y1="81.28" x2="45.72" y2="81.28" width="0.1524" layer="91"/>
<wire x1="48.26" y1="81.28" x2="53.34" y2="81.28" width="0.1524" layer="91"/>
<junction x="48.26" y="81.28"/>
<label x="50.8" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$18" gate="G$1" pin="VCCB"/>
<wire x1="68.58" y1="58.42" x2="63.5" y2="58.42" width="0.1524" layer="91"/>
<label x="60.96" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$19" gate="G$1" pin="VCCB"/>
<wire x1="68.58" y1="22.86" x2="63.5" y2="22.86" width="0.1524" layer="91"/>
<label x="60.96" y="22.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$20" gate="G$1" pin="5V"/>
<wire x1="68.58" y1="7.62" x2="63.5" y2="7.62" width="0.1524" layer="91"/>
<label x="63.5" y="7.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="IN1" class="0">
<segment>
<pinref part="U$17" gate="G$1" pin="GPIO17"/>
<wire x1="5.08" y1="68.58" x2="-5.08" y2="68.58" width="0.1524" layer="91"/>
<label x="-5.08" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$18" gate="G$1" pin="A1"/>
<wire x1="99.06" y1="81.28" x2="104.14" y2="81.28" width="0.1524" layer="91"/>
<label x="104.14" y="81.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="IN2" class="0">
<segment>
<pinref part="U$17" gate="G$1" pin="GPIO27"/>
<wire x1="5.08" y1="66.04" x2="-5.08" y2="66.04" width="0.1524" layer="91"/>
<label x="-5.08" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$18" gate="G$1" pin="A2"/>
<wire x1="99.06" y1="78.74" x2="104.14" y2="78.74" width="0.1524" layer="91"/>
<label x="104.14" y="78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="IN4" class="0">
<segment>
<pinref part="U$17" gate="G$1" pin="GPIO23"/>
<wire x1="45.72" y1="63.5" x2="53.34" y2="63.5" width="0.1524" layer="91"/>
<label x="50.8" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$18" gate="G$1" pin="A4"/>
<wire x1="99.06" y1="73.66" x2="104.14" y2="73.66" width="0.1524" layer="91"/>
<label x="104.14" y="73.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="IN6" class="0">
<segment>
<pinref part="U$17" gate="G$1" pin="GPIO5"/>
<wire x1="5.08" y1="45.72" x2="-5.08" y2="45.72" width="0.1524" layer="91"/>
<label x="-5.08" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$18" gate="G$1" pin="A6"/>
<wire x1="99.06" y1="68.58" x2="104.14" y2="68.58" width="0.1524" layer="91"/>
<label x="104.14" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="IN8" class="0">
<segment>
<pinref part="U$17" gate="G$1" pin="GPIO13"/>
<wire x1="5.08" y1="40.64" x2="-5.08" y2="40.64" width="0.1524" layer="91"/>
<label x="-5.08" y="40.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$18" gate="G$1" pin="A8"/>
<wire x1="99.06" y1="63.5" x2="104.14" y2="63.5" width="0.1524" layer="91"/>
<label x="104.14" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="IN10" class="0">
<segment>
<pinref part="U$17" gate="G$1" pin="GPIO26"/>
<wire x1="5.08" y1="35.56" x2="-5.08" y2="35.56" width="0.1524" layer="91"/>
<label x="-5.08" y="35.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$19" gate="G$1" pin="A2"/>
<wire x1="99.06" y1="43.18" x2="104.14" y2="43.18" width="0.1524" layer="91"/>
<label x="104.14" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="IN3" class="0">
<segment>
<pinref part="U$17" gate="G$1" pin="GPIO22"/>
<wire x1="5.08" y1="63.5" x2="-5.08" y2="63.5" width="0.1524" layer="91"/>
<label x="-5.08" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$18" gate="G$1" pin="A3"/>
<wire x1="99.06" y1="76.2" x2="104.14" y2="76.2" width="0.1524" layer="91"/>
<label x="104.14" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="IN5" class="0">
<segment>
<pinref part="U$17" gate="G$1" pin="GPIO24"/>
<wire x1="45.72" y1="60.96" x2="53.34" y2="60.96" width="0.1524" layer="91"/>
<label x="50.8" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$18" gate="G$1" pin="A5"/>
<wire x1="99.06" y1="71.12" x2="104.14" y2="71.12" width="0.1524" layer="91"/>
<label x="104.14" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="IN7" class="0">
<segment>
<pinref part="U$17" gate="G$1" pin="GPIO6"/>
<wire x1="5.08" y1="43.18" x2="-5.08" y2="43.18" width="0.1524" layer="91"/>
<label x="-5.08" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$18" gate="G$1" pin="A7"/>
<wire x1="99.06" y1="66.04" x2="104.14" y2="66.04" width="0.1524" layer="91"/>
<label x="104.14" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="IN9" class="0">
<segment>
<pinref part="U$17" gate="G$1" pin="GPIO19"/>
<wire x1="5.08" y1="38.1" x2="-5.08" y2="38.1" width="0.1524" layer="91"/>
<label x="-5.08" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$19" gate="G$1" pin="A1"/>
<wire x1="99.06" y1="45.72" x2="104.14" y2="45.72" width="0.1524" layer="91"/>
<label x="104.14" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="GND.1"/>
<wire x1="-22.86" y1="76.2" x2="-17.78" y2="76.2" width="0.1524" layer="91"/>
<label x="-17.78" y="76.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GND.2"/>
<wire x1="-22.86" y1="78.74" x2="-17.78" y2="78.74" width="0.1524" layer="91"/>
<label x="-17.78" y="78.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="GND"/>
<wire x1="48.26" y1="-22.86" x2="48.26" y2="-17.78" width="0.1524" layer="91"/>
<label x="48.26" y="-20.32" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GND"/>
<wire x1="-205.74" y1="55.88" x2="-213.36" y2="55.88" width="0.1524" layer="91"/>
<label x="-213.36" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="GND12"/>
<wire x1="81.28" y1="-22.86" x2="81.28" y2="-17.78" width="0.1524" layer="91"/>
<label x="81.28" y="-20.32" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="LMP10" gate="G$1" pin="1"/>
<wire x1="-63.5" y1="96.52" x2="-63.5" y2="93.98" width="0.1524" layer="91"/>
<pinref part="SUPPLY3" gate="GND" pin="GND"/>
<wire x1="-63.5" y1="93.98" x2="-43.18" y2="93.98" width="0.1524" layer="91"/>
<pinref part="LMP9" gate="G$1" pin="1"/>
<wire x1="-43.18" y1="93.98" x2="-30.48" y2="93.98" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="96.52" x2="-43.18" y2="93.98" width="0.1524" layer="91"/>
<junction x="-43.18" y="93.98"/>
</segment>
<segment>
<pinref part="LMP1" gate="G$1" pin="2"/>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
<wire x1="-182.88" y1="-2.54" x2="-182.88" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="-182.88" y1="-12.7" x2="-162.56" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="LMP2" gate="G$1" pin="2"/>
<wire x1="-162.56" y1="-12.7" x2="-142.24" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="-142.24" y1="-12.7" x2="-121.92" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="-121.92" y1="-12.7" x2="-116.84" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="-162.56" y1="-2.54" x2="-162.56" y2="-12.7" width="0.1524" layer="91"/>
<junction x="-162.56" y="-12.7"/>
<pinref part="LMP3" gate="G$1" pin="2"/>
<wire x1="-142.24" y1="-2.54" x2="-142.24" y2="-12.7" width="0.1524" layer="91"/>
<junction x="-142.24" y="-12.7"/>
<pinref part="LMP4" gate="G$1" pin="2"/>
<wire x1="-121.92" y1="-2.54" x2="-121.92" y2="-12.7" width="0.1524" layer="91"/>
<junction x="-121.92" y="-12.7"/>
<wire x1="-116.84" y1="-12.7" x2="-101.6" y2="-12.7" width="0.1524" layer="91"/>
<junction x="-116.84" y="-12.7"/>
<pinref part="LMP8" gate="G$1" pin="2"/>
<wire x1="-101.6" y1="-12.7" x2="-81.28" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="-81.28" y1="-12.7" x2="-60.96" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="-12.7" x2="-40.64" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="-12.7" x2="-40.64" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="LMP7" gate="G$1" pin="2"/>
<wire x1="-60.96" y1="-2.54" x2="-60.96" y2="-12.7" width="0.1524" layer="91"/>
<junction x="-60.96" y="-12.7"/>
<pinref part="LMP6" gate="G$1" pin="2"/>
<wire x1="-81.28" y1="-2.54" x2="-81.28" y2="-12.7" width="0.1524" layer="91"/>
<junction x="-81.28" y="-12.7"/>
<pinref part="LMP5" gate="G$1" pin="2"/>
<wire x1="-101.6" y1="-2.54" x2="-101.6" y2="-12.7" width="0.1524" layer="91"/>
<junction x="-101.6" y="-12.7"/>
<junction x="-182.88" y="-12.7"/>
<junction x="-40.64" y="-12.7"/>
</segment>
<segment>
<pinref part="U$13" gate="G$1" pin="GND"/>
<wire x1="35.56" y1="5.08" x2="30.48" y2="5.08" width="0.1524" layer="91"/>
<label x="30.48" y="5.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$13" gate="G$1" pin="GND."/>
<wire x1="35.56" y1="0" x2="30.48" y2="0" width="0.1524" layer="91"/>
<label x="30.48" y="0" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$14" gate="G$1" pin="GND"/>
<wire x1="-200.66" y1="-43.18" x2="-205.74" y2="-43.18" width="0.1524" layer="91"/>
<label x="-205.74" y="-43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$15" gate="G$1" pin="GND"/>
<wire x1="-175.26" y1="-43.18" x2="-180.34" y2="-43.18" width="0.1524" layer="91"/>
<label x="-180.34" y="-43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$17" gate="G$1" pin="GND2"/>
<wire x1="5.08" y1="71.12" x2="-2.54" y2="71.12" width="0.1524" layer="91"/>
<label x="-2.54" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$17" gate="G$1" pin="GND1"/>
<wire x1="45.72" y1="76.2" x2="48.26" y2="76.2" width="0.1524" layer="91"/>
<label x="48.26" y="76.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$17" gate="G$1" pin="GND5"/>
<wire x1="5.08" y1="50.8" x2="-2.54" y2="50.8" width="0.1524" layer="91"/>
<label x="-2.54" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$17" gate="G$1" pin="GND3"/>
<wire x1="45.72" y1="66.04" x2="48.26" y2="66.04" width="0.1524" layer="91"/>
<label x="48.26" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$17" gate="G$1" pin="GND4"/>
<wire x1="45.72" y1="58.42" x2="48.26" y2="58.42" width="0.1524" layer="91"/>
<label x="48.26" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$17" gate="G$1" pin="GND6"/>
<wire x1="45.72" y1="45.72" x2="48.26" y2="45.72" width="0.1524" layer="91"/>
<label x="48.26" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$17" gate="G$1" pin="GND7"/>
<wire x1="45.72" y1="40.64" x2="48.26" y2="40.64" width="0.1524" layer="91"/>
<label x="48.26" y="40.64" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="2.54" y1="33.02" x2="2.54" y2="27.94" width="0.1524" layer="91"/>
<pinref part="U$17" gate="G$1" pin="GND8"/>
<wire x1="5.08" y1="33.02" x2="2.54" y2="33.02" width="0.1524" layer="91"/>
<pinref part="X_1" gate="G$1" pin="0"/>
<label x="2.54" y="30.48" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U$18" gate="G$1" pin="GND"/>
<wire x1="99.06" y1="55.88" x2="104.14" y2="55.88" width="0.1524" layer="91"/>
<label x="104.14" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$19" gate="G$1" pin="GND"/>
<wire x1="99.06" y1="20.32" x2="104.14" y2="20.32" width="0.1524" layer="91"/>
<label x="104.14" y="20.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$20" gate="G$1" pin="GND"/>
<wire x1="68.58" y1="-2.54" x2="63.5" y2="-2.54" width="0.1524" layer="91"/>
<label x="63.5" y="-2.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="12V" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="DC12"/>
<wire x1="86.36" y1="-22.86" x2="86.36" y2="-17.78" width="0.1524" layer="91"/>
<label x="86.36" y="-20.32" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="U$13" gate="G$1" pin="+V1"/>
<wire x1="35.56" y1="-2.54" x2="30.48" y2="-2.54" width="0.1524" layer="91"/>
<label x="30.48" y="-2.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="VCC"/>
<wire x1="-205.74" y1="50.8" x2="-213.36" y2="50.8" width="0.1524" layer="91"/>
<label x="-213.36" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$14" gate="G$1" pin="VCC"/>
<wire x1="-200.66" y1="-38.1" x2="-205.74" y2="-38.1" width="0.1524" layer="91"/>
<label x="-205.74" y="-38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$15" gate="G$1" pin="VCC"/>
<wire x1="-175.26" y1="-38.1" x2="-180.34" y2="-38.1" width="0.1524" layer="91"/>
<label x="-180.34" y="-38.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="RX" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="RX"/>
<wire x1="43.18" y1="-22.86" x2="43.18" y2="-17.78" width="0.1524" layer="91"/>
<label x="43.18" y="-20.32" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="U$20" gate="G$1" pin="TXD"/>
<wire x1="68.58" y1="2.54" x2="63.5" y2="2.54" width="0.1524" layer="91"/>
<label x="63.5" y="2.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="TX" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="TX"/>
<wire x1="45.72" y1="-22.86" x2="45.72" y2="-17.78" width="0.1524" layer="91"/>
<label x="45.72" y="-20.32" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="U$20" gate="G$1" pin="RXD"/>
<wire x1="68.58" y1="0" x2="63.5" y2="0" width="0.1524" layer="91"/>
<label x="63.5" y="0" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="LMP10" gate="G$1" pin="2"/>
<pinref part="U$1" gate="G$1" pin="NO.K10"/>
<wire x1="-60.96" y1="96.52" x2="-60.96" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="LMP9" gate="G$1" pin="2"/>
<pinref part="U$1" gate="G$1" pin="NO.K9"/>
<wire x1="-40.64" y1="96.52" x2="-40.64" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="220V" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="COM.K8"/>
<wire x1="-187.96" y1="15.24" x2="-187.96" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="15.24" x2="-167.64" y2="15.24" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="COM.K7"/>
<wire x1="-167.64" y1="20.32" x2="-167.64" y2="15.24" width="0.1524" layer="91"/>
<junction x="-167.64" y="15.24"/>
<wire x1="-167.64" y1="15.24" x2="-147.32" y2="15.24" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="COM.K1"/>
<wire x1="-147.32" y1="15.24" x2="-127" y2="15.24" width="0.1524" layer="91"/>
<wire x1="-127" y1="15.24" x2="-106.68" y2="15.24" width="0.1524" layer="91"/>
<wire x1="-106.68" y1="15.24" x2="-86.36" y2="15.24" width="0.1524" layer="91"/>
<wire x1="-66.04" y1="15.24" x2="-45.72" y2="15.24" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="15.24" x2="-45.72" y2="20.32" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="COM.K2"/>
<wire x1="-66.04" y1="20.32" x2="-66.04" y2="15.24" width="0.1524" layer="91"/>
<junction x="-66.04" y="15.24"/>
<pinref part="U$1" gate="G$1" pin="COM.K3"/>
<wire x1="-66.04" y1="15.24" x2="-86.36" y2="15.24" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="COM.K4"/>
<wire x1="-106.68" y1="20.32" x2="-106.68" y2="15.24" width="0.1524" layer="91"/>
<junction x="-106.68" y="15.24"/>
<wire x1="-86.36" y1="20.32" x2="-86.36" y2="15.24" width="0.1524" layer="91"/>
<junction x="-86.36" y="15.24"/>
<pinref part="U$1" gate="G$1" pin="COM.K5"/>
<wire x1="-127" y1="20.32" x2="-127" y2="15.24" width="0.1524" layer="91"/>
<junction x="-127" y="15.24"/>
<pinref part="U$1" gate="G$1" pin="COM.K6"/>
<wire x1="-147.32" y1="20.32" x2="-147.32" y2="15.24" width="0.1524" layer="91"/>
<junction x="-147.32" y="15.24"/>
<label x="-208.28" y="17.78" size="1.778" layer="95" rot="R180"/>
<junction x="-45.72" y="15.24"/>
<wire x1="-187.96" y1="15.24" x2="-213.36" y2="15.24" width="0.1524" layer="91"/>
<junction x="-187.96" y="15.24"/>
</segment>
<segment>
<wire x1="-78.74" y1="91.44" x2="-66.04" y2="91.44" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="COM.K9"/>
<wire x1="-66.04" y1="91.44" x2="-45.72" y2="91.44" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="91.44" x2="-45.72" y2="88.9" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="COM.K10"/>
<wire x1="-66.04" y1="91.44" x2="-66.04" y2="88.9" width="0.1524" layer="91"/>
<junction x="-66.04" y="91.44"/>
<label x="-73.66" y="93.98" size="1.778" layer="95" rot="R180"/>
<junction x="-45.72" y="91.44"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="NO.K8"/>
<wire x1="-193.04" y1="20.32" x2="-193.04" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-193.04" y1="-7.62" x2="-185.42" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="LMP1" gate="G$1" pin="1"/>
<wire x1="-185.42" y1="-7.62" x2="-185.42" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="NO.K7"/>
<wire x1="-172.72" y1="20.32" x2="-172.72" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="-7.62" x2="-165.1" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="LMP2" gate="G$1" pin="1"/>
<wire x1="-165.1" y1="-7.62" x2="-165.1" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="NO.K6"/>
<wire x1="-152.4" y1="20.32" x2="-152.4" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-152.4" y1="-7.62" x2="-144.78" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="LMP3" gate="G$1" pin="1"/>
<wire x1="-144.78" y1="-7.62" x2="-144.78" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="NO.K5"/>
<wire x1="-132.08" y1="20.32" x2="-132.08" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="LMP4" gate="G$1" pin="1"/>
<wire x1="-132.08" y1="-7.62" x2="-124.46" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-124.46" y1="-7.62" x2="-124.46" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="NO.K4"/>
<wire x1="-111.76" y1="20.32" x2="-111.76" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="LMP5" gate="G$1" pin="1"/>
<wire x1="-111.76" y1="-7.62" x2="-104.14" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-104.14" y1="-7.62" x2="-104.14" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="NO.K3"/>
<wire x1="-91.44" y1="20.32" x2="-91.44" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-91.44" y1="-7.62" x2="-83.82" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="LMP6" gate="G$1" pin="1"/>
<wire x1="-83.82" y1="-7.62" x2="-83.82" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="NO.K2"/>
<wire x1="-71.12" y1="20.32" x2="-71.12" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="-7.62" x2="-63.5" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="LMP7" gate="G$1" pin="1"/>
<wire x1="-63.5" y1="-7.62" x2="-63.5" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="LMP8" gate="G$1" pin="1"/>
<wire x1="-43.18" y1="-7.62" x2="-43.18" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="NO.K1"/>
<wire x1="-50.8" y1="20.32" x2="-50.8" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="-7.62" x2="-43.18" y2="-7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="(-)CH10"/>
<pinref part="U$3" gate="G$1" pin="GND"/>
<wire x1="-109.22" y1="-60.96" x2="-109.22" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="(+)CH10"/>
<pinref part="U$3" gate="G$1" pin="VCC"/>
<wire x1="-106.68" y1="-60.96" x2="-106.68" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="F.INPUT.CH10"/>
<pinref part="U$3" gate="G$1" pin="FIN"/>
<wire x1="-104.14" y1="-60.96" x2="-104.14" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="F.COM.CH10"/>
<pinref part="U$3" gate="G$1" pin="FCOM"/>
<wire x1="-101.6" y1="-60.96" x2="-101.6" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="(-)CH9"/>
<pinref part="U$4" gate="G$1" pin="GND"/>
<wire x1="-88.9" y1="-60.96" x2="-88.9" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="(+)CH9"/>
<pinref part="U$4" gate="G$1" pin="VCC"/>
<wire x1="-86.36" y1="-60.96" x2="-86.36" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="F.INPUT.CH9"/>
<pinref part="U$4" gate="G$1" pin="FIN"/>
<wire x1="-83.82" y1="-60.96" x2="-83.82" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="F.COM.CH9"/>
<pinref part="U$4" gate="G$1" pin="FCOM"/>
<wire x1="-81.28" y1="-60.96" x2="-81.28" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="(-)CH8"/>
<pinref part="U$5" gate="G$1" pin="GND"/>
<wire x1="-68.58" y1="-60.96" x2="-68.58" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="(+)CH8"/>
<pinref part="U$5" gate="G$1" pin="VCC"/>
<wire x1="-66.04" y1="-60.96" x2="-66.04" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="F.INPUT.CH8"/>
<pinref part="U$5" gate="G$1" pin="FIN"/>
<wire x1="-63.5" y1="-60.96" x2="-63.5" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="F.COM.CH8"/>
<pinref part="U$5" gate="G$1" pin="FCOM"/>
<wire x1="-60.96" y1="-60.96" x2="-60.96" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="(-)CH7"/>
<pinref part="U$6" gate="G$1" pin="GND"/>
<wire x1="-48.26" y1="-60.96" x2="-48.26" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="(+)CH7"/>
<pinref part="U$6" gate="G$1" pin="VCC"/>
<wire x1="-45.72" y1="-60.96" x2="-45.72" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="F.INPUT.CH7"/>
<pinref part="U$6" gate="G$1" pin="FIN"/>
<wire x1="-43.18" y1="-60.96" x2="-43.18" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="F.COM.CH7"/>
<pinref part="U$6" gate="G$1" pin="FCOM"/>
<wire x1="-40.64" y1="-60.96" x2="-40.64" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="(-)CH6"/>
<pinref part="U$7" gate="G$1" pin="GND"/>
<wire x1="-27.94" y1="-60.96" x2="-27.94" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="(+)CH6"/>
<pinref part="U$7" gate="G$1" pin="VCC"/>
<wire x1="-25.4" y1="-60.96" x2="-25.4" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="F.INPUT.CH6"/>
<pinref part="U$7" gate="G$1" pin="FIN"/>
<wire x1="-22.86" y1="-60.96" x2="-22.86" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="F.COM.CH6"/>
<pinref part="U$7" gate="G$1" pin="FCOM"/>
<wire x1="-20.32" y1="-60.96" x2="-20.32" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="(-)CH5"/>
<pinref part="U$8" gate="G$1" pin="GND"/>
<wire x1="-7.62" y1="-60.96" x2="-7.62" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="(+)CH5"/>
<pinref part="U$8" gate="G$1" pin="VCC"/>
<wire x1="-5.08" y1="-60.96" x2="-5.08" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="F.INPUT.CH5"/>
<pinref part="U$8" gate="G$1" pin="FIN"/>
<wire x1="-2.54" y1="-60.96" x2="-2.54" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="F.COM.CH5"/>
<pinref part="U$8" gate="G$1" pin="FCOM"/>
<wire x1="0" y1="-60.96" x2="0" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="(-)CH4"/>
<pinref part="U$9" gate="G$1" pin="GND"/>
<wire x1="12.7" y1="-60.96" x2="12.7" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="(+)CH4"/>
<pinref part="U$9" gate="G$1" pin="VCC"/>
<wire x1="15.24" y1="-60.96" x2="15.24" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="F.INPUT.CH4"/>
<pinref part="U$9" gate="G$1" pin="FIN"/>
<wire x1="17.78" y1="-60.96" x2="17.78" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="F.COM.CH4"/>
<pinref part="U$9" gate="G$1" pin="FCOM"/>
<wire x1="20.32" y1="-60.96" x2="20.32" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="(-)CH3"/>
<pinref part="U$10" gate="G$1" pin="GND"/>
<wire x1="33.02" y1="-60.96" x2="33.02" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="(+)CH3"/>
<pinref part="U$10" gate="G$1" pin="VCC"/>
<wire x1="35.56" y1="-60.96" x2="35.56" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="F.INPUT.CH3"/>
<pinref part="U$10" gate="G$1" pin="FIN"/>
<wire x1="38.1" y1="-60.96" x2="38.1" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="F.COM.CH3"/>
<pinref part="U$10" gate="G$1" pin="FCOM"/>
<wire x1="40.64" y1="-60.96" x2="40.64" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="(-)CH2"/>
<pinref part="U$11" gate="G$1" pin="GND"/>
<wire x1="53.34" y1="-60.96" x2="53.34" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="(+)CH2"/>
<pinref part="U$11" gate="G$1" pin="VCC"/>
<wire x1="55.88" y1="-60.96" x2="55.88" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="F.INPUT.CH2"/>
<pinref part="U$11" gate="G$1" pin="FIN"/>
<wire x1="58.42" y1="-60.96" x2="58.42" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="F.COM.CH2"/>
<pinref part="U$11" gate="G$1" pin="FCOM"/>
<wire x1="60.96" y1="-60.96" x2="60.96" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="(-)CH1"/>
<pinref part="U$12" gate="G$1" pin="GND"/>
<wire x1="73.66" y1="-60.96" x2="73.66" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="(+)CH1"/>
<pinref part="U$12" gate="G$1" pin="VCC"/>
<wire x1="76.2" y1="-60.96" x2="76.2" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="F.INPUT.CH1"/>
<pinref part="U$12" gate="G$1" pin="FIN"/>
<wire x1="78.74" y1="-60.96" x2="78.74" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="F.COM.CH1"/>
<pinref part="U$12" gate="G$1" pin="FCOM"/>
<wire x1="81.28" y1="-60.96" x2="81.28" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VAC(+)" class="0">
<segment>
<pinref part="U$16" gate="G$1" pin="L(+)"/>
<wire x1="15.24" y1="7.62" x2="20.32" y2="7.62" width="0.1524" layer="91"/>
<label x="17.78" y="7.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="VAC(-)" class="0">
<segment>
<pinref part="U$16" gate="G$1" pin="N(-)"/>
<wire x1="15.24" y1="5.08" x2="20.32" y2="5.08" width="0.1524" layer="91"/>
<label x="17.78" y="5.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="VAC(FG)" class="0">
<segment>
<pinref part="U$16" gate="G$1" pin="FG"/>
<wire x1="15.24" y1="2.54" x2="20.32" y2="2.54" width="0.1524" layer="91"/>
<label x="17.78" y="2.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="L(+)" class="0">
<segment>
<pinref part="U$16" gate="G$1" pin="L(+)OUT1"/>
<wire x1="-15.24" y1="17.78" x2="-22.86" y2="17.78" width="0.1524" layer="91"/>
<label x="-22.86" y="17.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$13" gate="G$1" pin="L(+)"/>
<wire x1="35.56" y1="12.7" x2="30.48" y2="12.7" width="0.1524" layer="91"/>
<label x="30.48" y="12.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="N(-)" class="0">
<segment>
<pinref part="U$16" gate="G$1" pin="N(-)OUT1"/>
<wire x1="-15.24" y1="15.24" x2="-22.86" y2="15.24" width="0.1524" layer="91"/>
<label x="-22.86" y="15.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$13" gate="G$1" pin="N(-)"/>
<wire x1="35.56" y1="10.16" x2="30.48" y2="10.16" width="0.1524" layer="91"/>
<label x="30.48" y="10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="FG" class="0">
<segment>
<pinref part="U$16" gate="G$1" pin="FG.OUT1"/>
<wire x1="-15.24" y1="12.7" x2="-22.86" y2="12.7" width="0.1524" layer="91"/>
<label x="-22.86" y="12.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$13" gate="G$1" pin="FG"/>
<wire x1="35.56" y1="7.62" x2="30.48" y2="7.62" width="0.1524" layer="91"/>
<label x="30.48" y="7.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="OUT10" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="IN10"/>
<wire x1="-22.86" y1="58.42" x2="-17.78" y2="58.42" width="0.1524" layer="91"/>
<label x="-17.78" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$19" gate="G$1" pin="B2"/>
<wire x1="68.58" y1="43.18" x2="63.5" y2="43.18" width="0.1524" layer="91"/>
<label x="60.96" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="OUT9" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="IN9"/>
<wire x1="-22.86" y1="55.88" x2="-17.78" y2="55.88" width="0.1524" layer="91"/>
<label x="-17.78" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$19" gate="G$1" pin="B1"/>
<wire x1="68.58" y1="45.72" x2="63.5" y2="45.72" width="0.1524" layer="91"/>
<label x="60.96" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="OUT8" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="IN8"/>
<wire x1="-22.86" y1="53.34" x2="-17.78" y2="53.34" width="0.1524" layer="91"/>
<label x="-17.78" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$18" gate="G$1" pin="B8"/>
<wire x1="68.58" y1="63.5" x2="63.5" y2="63.5" width="0.1524" layer="91"/>
<label x="60.96" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="OUT7" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="IN7"/>
<wire x1="-22.86" y1="50.8" x2="-17.78" y2="50.8" width="0.1524" layer="91"/>
<label x="-17.78" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$18" gate="G$1" pin="B7"/>
<wire x1="68.58" y1="66.04" x2="63.5" y2="66.04" width="0.1524" layer="91"/>
<label x="60.96" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="OUT6" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="IN6"/>
<wire x1="-22.86" y1="48.26" x2="-17.78" y2="48.26" width="0.1524" layer="91"/>
<label x="-17.78" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$18" gate="G$1" pin="B6"/>
<wire x1="68.58" y1="68.58" x2="63.5" y2="68.58" width="0.1524" layer="91"/>
<label x="60.96" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="OUT5" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="IN5"/>
<wire x1="-22.86" y1="45.72" x2="-17.78" y2="45.72" width="0.1524" layer="91"/>
<label x="-17.78" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$18" gate="G$1" pin="B5"/>
<wire x1="68.58" y1="71.12" x2="63.5" y2="71.12" width="0.1524" layer="91"/>
<label x="60.96" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="OUT4" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="IN4"/>
<wire x1="-22.86" y1="43.18" x2="-17.78" y2="43.18" width="0.1524" layer="91"/>
<label x="-17.78" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$18" gate="G$1" pin="B4"/>
<wire x1="68.58" y1="73.66" x2="63.5" y2="73.66" width="0.1524" layer="91"/>
<label x="60.96" y="73.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="OUT3" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="IN3"/>
<wire x1="-22.86" y1="40.64" x2="-17.78" y2="40.64" width="0.1524" layer="91"/>
<label x="-17.78" y="40.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$18" gate="G$1" pin="B3"/>
<wire x1="68.58" y1="76.2" x2="63.5" y2="76.2" width="0.1524" layer="91"/>
<label x="60.96" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="OUT2" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="IN2"/>
<wire x1="-22.86" y1="38.1" x2="-17.78" y2="38.1" width="0.1524" layer="91"/>
<label x="-17.78" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$18" gate="G$1" pin="B2"/>
<wire x1="68.58" y1="78.74" x2="63.5" y2="78.74" width="0.1524" layer="91"/>
<label x="60.96" y="78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="OUT1" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="IN1"/>
<wire x1="-22.86" y1="35.56" x2="-17.78" y2="35.56" width="0.1524" layer="91"/>
<label x="-17.78" y="35.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$18" gate="G$1" pin="B1"/>
<wire x1="68.58" y1="81.28" x2="63.5" y2="81.28" width="0.1524" layer="91"/>
<label x="60.96" y="81.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="3.3V" class="0">
<segment>
<pinref part="U$18" gate="G$1" pin="VCCA"/>
<wire x1="68.58" y1="55.88" x2="63.5" y2="55.88" width="0.1524" layer="91"/>
<label x="60.96" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$19" gate="G$1" pin="VCCA"/>
<wire x1="68.58" y1="20.32" x2="63.5" y2="20.32" width="0.1524" layer="91"/>
<label x="60.96" y="20.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$18" gate="G$1" pin="OE"/>
<wire x1="99.06" y1="58.42" x2="104.14" y2="58.42" width="0.1524" layer="91"/>
<label x="104.14" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$19" gate="G$1" pin="OE"/>
<wire x1="99.06" y1="22.86" x2="104.14" y2="22.86" width="0.1524" layer="91"/>
<label x="104.14" y="22.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$17" gate="G$1" pin="3.3V"/>
<wire x1="5.08" y1="81.28" x2="-5.08" y2="81.28" width="0.1524" layer="91"/>
<label x="-5.08" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$17" gate="G$1" pin="3.3V."/>
<wire x1="5.08" y1="60.96" x2="-5.08" y2="60.96" width="0.1524" layer="91"/>
<label x="-5.08" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="USB" class="0">
<segment>
<pinref part="U$20" gate="G$1" pin="USB"/>
<wire x1="93.98" y1="2.54" x2="99.06" y2="2.54" width="0.1524" layer="91"/>
<label x="93.98" y="2.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$17" gate="G$1" pin="USB"/>
<wire x1="25.4" y1="25.4" x2="25.4" y2="22.86" width="0.1524" layer="91"/>
<label x="25.4" y="22.86" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
